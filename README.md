# Text and Graph Denoising Pre-Training for RDF-to-Text Generation

This is the repository for the Université de Lorraine M2 NLP thesis of Joseph Keenan.

## Repository Structure:
- `data_interface` is a directory that has to do with handling data during training 
  - `dataset_classes.py` contains the classes to shuffle, preprocess, and feed the data into our model during training
- `model_interface` is a directory containing scripts to instantiate the huggingface models. we have local versions of these files so changes can easily be made if necessary.
  - `modeling_bart.py` contains the BART model
  - `modeling_t5.py` contains the T5 model
  - `modeling_mt5.py` contains the MT5 model
  - `modeling_t5_adapter.py` contains the T5 model from the [AdapterHub](https://adapterhub.ml) which easily enables parameter efficient fine-tuning using T5
- `prep_data` is a directory containing scripts to preprocessing WebNLG and KELM data
  - `kelm_data_split.py` is a script to create the dataset splits for KELM
  - `kelm_property_filter.py` is a script to create a subset of KELM according to the distribution of properties
  - `kelm_serialize.py` is a script to serialize the KELM dataset
  - `rel.py` is a script to run the [REL Entity Linker](https://github.com/informagi/REL)
  - `webnlg_enriched_reader.py` is a reader for the enriched WebNLG data
  - `webnlg_enriched_serialize.py` is a script to serialize the enriched WebNLG data
  - `webnlg_entity_organization.py` is a script to organize the WebNLG entities after they have been found using the algorithm from [Entity Based Semantic Adequacy](https://gitlab.nl4xai.eu/juliette.faille/entity-based-semantic-adequacy)
  - `webnlg_serialize.py` is a script to serialize the WebNLG data, which is a modified version of [this script](https://github.com/UKPLab/plms-graph2text/blob/master/webnlg/data/generate_input_webnlg.py)
- `utils`
  - `general.py` is a script containing several general utility functions
  - `masking_functions.py` contains the masking functions used for pre-training
  - `organize_preds.py` is a script to organize the predictions into text files
  - `zeroto32_conversion.py` is a script to get fp32 weights from a Deepspeed checkpoint, it is a modified version of [this script](https://github.com/microsoft/DeepSpeed/blob/master/deepspeed/utils/zero_to_fp32.py)
- `bert_viz_test.py` is the script used to calculate the average graph and text attention scores for a given input
- `lightning_modules.py` contains the training and data modules to run the training loop and create dataloaders
- `finetuning_lightning.py` is script to run fine-tuning
- `inference_lightning.py` is script to run inference
- `training_lightning.py` is script to run training
- `run-lightning-finetuning-init.sh` is a bash script to run `finetuning_lightning.py`
- `run-lightning-inference-init.sh` is a bash script to run `inference_lightning.py`
- `run-lightning-training-init.sh` is a bash script to run `training_lightning.py`
- `requirements.yml` is a file containing the requirements to create a conda environment that can run all of the scripts in this repository

## Data
- The WebNLG dataset can be found [here](https://gitlab.com/shimorina/webnlg-dataset)
  - We use version 3.0.
- The Enriched WebNLG dataset can be found [here](https://github.com/ThiagoCF05/webnlg)
  - We use version 1.6.
- The KELM corpus can be found [here](https://github.com/google-research-datasets/KELM-corpus)

## Running the scripts
### Data Preprocessing
- To preprocess the normal WebNLG dataset you can use [this](https://gitlab.com/webnlg/corpus-reader) corpus reader
- To preprocess Enriched WebNLG you can follow the instructions for [this](https://github.com/zhijing-jin/WebNLG_Reader/) reader but using our modified version of the script `webnlg_enriched_reader.py`
- To preprocess KELM you will have to run `kelm_property_filter.py` to create a subset of the full dataset, then you can run `kelm_data_split.py` to split and serialize the data.
- To get the entity mentions for a specific dataset you can run the algorithm from [Entity Based Semantic Adequacy](https://gitlab.nl4xai.eu/juliette.faille/entity-based-semantic-adequacy), then you can organize the results using `webnlg_entity_organization.py`

Note that the way I have written the dataloaders, the model expects txt files named `.source file` with the serialized RDF graphs and `.target file` with the reference texts for each dataset split, where each graph or text is on it's own line. With the file structure generally organized as such:
```
├── data
    ├── train.source
    ├── train.target
    ├── val.source
    ├── val.target
    ├── test_both.source
    ├── test_both.target
    ├── test_seen.source
    ├── test_seen.target
    ├── test_unseen.source
    ├── test_unseen.target
```

### Pre-training and Fine-tuning
To run pre-training modify the `run-lightning-training-init.sh` file using the hyperparameters that you would like and your filepaths then run the script as such:
```
bash run-lightning-training-init.sh
```
The same procedure goes for fine-tuning and inference, just use the fine-tuning and inference scripts rather than the training one.

### Evaluation
To run evaluation you can follow the instructions for the official WebNLG Challenge Evaluation scripts [here](https://github.com/WebNLG/GenerationEval/tree/da996e951b35b72d0684f2234844912604e04885)
