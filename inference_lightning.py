import os
import re
from argparse import ArgumentParser
from transformers import BartTokenizer, T5Tokenizer, MT5Tokenizer, AutoTokenizer
from lightning_modules import LitRDF2TextBART, LitRDF2TextDataModule, CustomWriter
from data_interface.dataset_classes import DataCollatorForSeq2SeqWithMaskingAndPadding
from pytorch_lightning import Trainer
import pytorch_lightning.callbacks as cb
from pytorch_lightning.loggers import TensorBoardLogger
from utils.organize_preds import find_pred_files, organize_preds

def main(args):


    # Define the tokenizer
    if 'bart' in args.model_name_or_path:
        tok = AutoTokenizer.from_pretrained(args.model_name_or_path)
        new_special_tokens = {'additional_special_tokens': ['<g>', '</g>']}
        tok.add_special_tokens(new_special_tokens)
    elif 't5' in args.model_name_or_path:
        tok = AutoTokenizer.from_pretrained(args.model_name_or_path) 
        new_special_tokens = {'bos_token': '<s>', 'mask_token': '<mask>', 'additional_special_tokens': ['<g>', '</g>']}
        tok.add_special_tokens(new_special_tokens)

    # Instantiate prediction writer
    # ckpt_name = args.ckpt_path
    # if 'version' in ckpt_name:
        # pattern = re.findall('version_[0-9][0-9]?', ckpt_name)[0]
    # elif 'webnlg' in ckpt_name:
        # pattern = re.findall('webnlg_pt[0-9][0-9]?', ckpt_name)[0]
    output_dir = f'/home/jkeenan/low_resource_rdf2text/predictions_{args.experiment_name}/'
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    pred_writer = CustomWriter(output_dir=output_dir, write_interval='epoch')

    # Instantiate the trainer
    trainer = Trainer.from_argparse_args(args,
                                        callbacks=[pred_writer])
    
    rank = trainer.global_rank
    world_size = trainer.world_size

    # Ensure that the max_length variable is set and correct
    if not args.max_length or args.max_length > tok.model_max_length:
        args.max_length = tok.model_max_length

    # Instantiate the model, data collator, and data module
    # Here we are using the load_from_checkpoint method to load the model, but passing the new arguments (args), since we will want inference and not finetuning parameters (i.e. generation tasks)
    model = LitRDF2TextBART.load_from_checkpoint(args.ckpt_path, args=args, finetune=False)

    collate_fn = DataCollatorForSeq2SeqWithMaskingAndPadding(tokenizer=tok, max_length=args.max_length, padding=True)
    
    dataset = LitRDF2TextDataModule(args, tokenizer=tok, collate_fn=collate_fn, rank=rank, world_size=world_size)

    # Preprocess the data and organize it into dataloaders
    # dataset.prepare_data(test=True)
    dataset.setup('predict')
    data_test = dataset.test_dataloader()

    # Run inference on the model
    trainer.predict(model, dataloaders=data_test)

    print('Done running inference. Organizing predictions...')
    pred_files = find_pred_files(output_dir, '.pt')
    organize_preds(pred_files=pred_files, save_path=output_dir)

if __name__ == "__main__":
    parser = ArgumentParser()

    #Add General arguments
    parser.add_argument(
        "--dataset",
        default=None,
        type=str,
        required=True,
        help='Name of the dataset being used; This is only used to add the dataset name to the output directory'
    )
    parser.add_argument(
        "--output_dir",
        type=str,
        required=False,
        help="The output directory where the model predictions and checkpoints will be written.",
    )
    parser.add_argument(
        "--max_length",
        default=1024,
        type=int,
        help="maximum length of input sequences"
    )
    parser.add_argument(
        "--gradient_accumulation_steps",
        type=int,
        default=1,
        help="Number of updates steps to accumulate before performing a backward/update pass.",
    )
    parser.add_argument("--patience", 
        default=20,
        type=int,
        help="Number of validation epochs to keep training without improvement before early stopping")
    parser.add_argument(
        "--stop_criteria",
        type=str,
        help="Criteria used for saving checkpoints and early stopping in training"
    )
    parser.add_argument("--seed", type=int, default=42, help="random seed for initialization")

    # Add model specific arguments
    parser = LitRDF2TextBART.add_model_specific_args(parser)

    # Add data module specific arguments
    parser = LitRDF2TextDataModule.add_data_specific_args(parser)

    # Add all the available trainer options to argparse
    parser = Trainer.add_argparse_args(parser)

    # Parse all of the arguments
    args = parser.parse_args()

    # Run the main function
    main(args)
