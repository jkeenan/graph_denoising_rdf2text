#!/bin/bash

source ~/.bashrc
conda activate rdf2text

cd /home/jkeenan/low_resource_rdf2text/

exp_name="webnlg_enriched_baseline_pt18"
dataset=webnlg
datapath=/home/jkeenan/${dataset}_serialized

# model_size="large"
# MODEL="facebook/bart-$model_size"
model_size="base"
MODEL="t5-$model_size"
ckpt="/home/jkeenan/low_resource_rdf2text/lightning_logs/webnlg_enriched_baseline_pt18/version_0/checkpoints/epoch=78-val_loss=0.052.pt"
lr=2e-5
# lr=3.3e-5
stop_criteria='val_loss'

python finetuning_lightning.py \
  --dataset $dataset \
  --data_path $datapath \
  --graph2text  \
  --train_batch_size 32 \
  --eval_batch_size 64 \
  --model_name_or_path $MODEL \
  --ckpt_path $ckpt \
  --max_epochs 100  \
  --learning_rate $lr \
  --val_check_interval 550 \
  --predict_num_beams 5 \
  --sent_max_gen_length 200 \
  --buffer_size 1000 \
  --patience 20 \
  --stop_criteria $stop_criteria  \
  --accelerator gpu \
  --strategy deepspeed_stage_2 \
  --precision bf16 \
  --seed 21 \
  --experiment_name $exp_name \
  # --accumulate_grad_batches 2 \