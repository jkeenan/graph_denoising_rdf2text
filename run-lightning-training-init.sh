#!/bin/bash

source ~/.bashrc
conda activate rdf2text_2.0

cd /home/jkeenan/low_resource_rdf2text/

exp_name="webnlg_enriched_baseline_pt18"
dataset=webnlg
datapath=/home/jkeenan/${dataset}_enriched_serialized_tok
# datapath=/home/jkeenan/${dataset}_dummy

# model_size="large"
# MODEL="facebook/bart-$model_size"
model_size="base"
MODEL="t5-$model_size"
augmented_interval=1
joint_interval=1

lr=5e-5
# lr=6.3e-6
stop_criteria="val_loss"

# outpath=outputs/${dataset}-bart-${model_size}-UnifiedTextDenoising-lightning
# mkdir -p $outpath
# echo "OutputDir: $outpath"

python training_lightning.py \
  --dataset $dataset \
  --data_path $datapath \
  --mlm_rdf \
  --mlm_rdf_plus_text  \
  --mlm_joint_to_rdf  \
  --train_batch_size 32 \
  --eval_batch_size 32 \
  --model_name_or_path $MODEL \
  --max_epochs 100 \
  --max_steps 100000 \
  --learning_rate $lr \
  --augmented_train_interval $augmented_interval \
  --joint_train_interval $joint_interval \
  --dynamic_augmented_masking \
  --warmup_steps 2500 \
  --val_check_interval 139 \
  --predict_num_beams 5 \
  --buffer_size 1000 \
  --patience 20 \
  --mlm_probability 0.3 \
  --stop_criteria $stop_criteria  \
  --accelerator gpu \
  --strategy deepspeed_stage_2 \
  --precision bf16 \
  --seed 13 \
  --accumulate_grad_batches 4 \
  --experiment_name $exp_name \
  # --warmup_steps 2500 \
  # --precision bf16 \
