from typing import List, Union
import pytorch_lightning as pl
from pytorch_lightning.callbacks import BasePredictionWriter
import argparse
import os

from pytorch_lightning.utilities.types import EPOCH_OUTPUT
from data_interface.dataset_classes import CustomDataset, ShuffleDataset
from model_interface.modeling_bart import BartForConditionalGeneration
from model_interface.modeling_t5 import T5ForConditionalGeneration
# from model_interface.modeling_t5_adapter import T5ForConditionalGeneration
# from transformers.adapters import PrefixTuningConfig
# import transformers.adapters.composition as ac
from model_interface.modeling_mt5 import MT5ForConditionalGeneration
from utils.general import get_inverse_sqrt_schedule_with_warmup, save_dummy_batch
# from datasets import load_dataset, Features, Value, Translation, interleave_datasets
# from datasets.distributed import split_dataset_by_node
# from torchdata.dataloader2 import DataLoader2
# from torchdata.dataloader2.reading_service import DistributedReadingService, MultiProcessingReadingService, SequentialReadingService

import torch
from torch.utils.data import DataLoader
from torch.optim import AdamW
from utils.masking_functions import MaskMaster, get_generation, get_entity_lex


# Pytorch Lightning Model Class for RDF2Text pretraining and finetuning
class LitRDF2TextBART(pl.LightningModule):
    def __init__(self,
                 args: argparse.Namespace,
                 tokenizer: str,
                 finetune: bool,
                 **kwargs,
                 ):
        super().__init__()
        self.save_hyperparameters()  # saves all the arguments passed to the class
        self.args = args  # arguments passed to the class via argparse
        self.tokenizer = tokenizer  # tokenizer to use for tokenization
        self.finetune = finetune  # whether we are finetuning or not; changes the optimizer configuration
        if 'bart' in self.args.model_name_or_path:
            self.model = BartForConditionalGeneration.from_pretrained(self.args.model_name_or_path)  # initialize model
        elif 't5' in self.args.model_name_or_path:
            self.model = T5ForConditionalGeneration.from_pretrained(self.args.model_name_or_path)
            if self.args.activate_adapter:
                print('Training an adapter rather than full model finetuning\nAcitvating Adapter...\nUpdating learning rate to 1e-4...')
                if self.args.adapter_ckpt_path:
                    print('Loading pretrained model & adapter...')
                    self.model = T5ForConditionalGeneration.from_pretrained(self.args.ckpt_path)
                    self.model.load_adapter(self.args.adapter_ckpt_path)
                else:
                    print('Training adapter from scratch...')
                    if 'prefix' in self.args.adapter_config:
                        # I have further defined the prefix tuning config to be able to set my own prefix length
                        config = PrefixTuningConfig(flat=False, prefix_length=self.args.prefix_length)
                    else:
                        # For any config other than prefix tuning I just use the default config which can be activated by entering the string name of the method in the self.add_adapter method below
                        config = self.args.adapter_config
                    self.model.add_adapter(self.args.adapter_config, config=config)
                self.model.set_active_adapters(self.args.adapter_config)
                if not self.args.adapter_inference:
                    self.model.train_adapter(self.args.adapter_config)
                    self.args.learning_rate = 1e-4
        # Resize token embedding since we added new special tokens for the beginnning and end of a graph
        self.model.resize_token_embeddings(len(self.tokenizer))
        self.predict_num_beams = self.args.predict_num_beams  # number of beams to use for beam search during inference
        self.learning_rate = self.args.learning_rate  # learning rate for the optimizer
        self.noise_functions = []
        self.mask_weights = []
        self.min_val_loss = 1000
        self.mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=self.args.mlm_probability,
                               mask_entities=self.args.mask_entities, mask_properties=self.args.mask_properties,
                               span_masking=self.args.span_masking)
        if self.args.mask_entities and self.args.mask_properties:
            self.entity_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=self.args.mlm_probability,
                               mask_entities=True, mask_properties=False, span_masking=self.args.entity_span_masking)
            # span masking is instantiated for property masking since it performs better than individual subtoken masks
            self.property_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=self.args.mlm_probability,
                               mask_entities=False, mask_properties=True, span_masking=self.args.property_span_masking)
            self.noise_functions.append(self.entity_mask)
            self.noise_functions.append(self.property_mask)
            if self.args.entity_masking_weight and self.args.property_masking_weight:
                try:
                    assert self.args.entity_masking_weight + self.args.property_masking_weight == 1
                except AssertionError:
                    print('Entity & Property Masking weights do not add up to 1.\nSetting new masking weights to entity_masking_weight and 1 - entity_masking_weight\nfor entity and property masking weights respectively.')
                    self.args.property_masking_weight = 1 - self.args.entity_masking_weight
                self.mask_weights.append(self.args.entity_masking_weight)
                self.mask_weights.append(self.args.property_masking_weight)
            else:
                self.mask_weights.append(1)
                self.mask_weights.append(1)
        else:
            self.noise_functions.append(self.mask)
            self.mask_weights.append(1)
        


    @staticmethod
    def add_model_specific_args(parent_parser):
        '''
        Method to add model specific arguments
        '''
        # General model arguments
        parser = parent_parser.add_argument_group("LitModel")
        parser.add_argument("--adam_epsilon", default=1e-8, type=float, help="Epsilon for Adam optimizer.")
        parser.add_argument("--ckpt_path", default=None, type=str,
                            help="The model checkpoint that was saved during the pretraining")
        parser.add_argument("--adapter_ckpt_path", default=None, type=str,
                            help="The model adapter checkpoint that was saved during the pretraining")
        parser.add_argument("--adapter_inference", action="store_true",
                            help="Argument to load the trained model adapter for inference")
        parser.add_argument("--augmented_train_interval", default=1, type=int, help="The interval of [Masked Text + Graph -> Text] & [Text + Masked Graph -> Graph] training")
        parser.add_argument("--joint_train_interval", default=1, type=int, help="The interval of [Masked Text + Masked Graph -> Text] & [Masked Text + Masked Graph -> Graph] training")
        parser.add_argument("--learning_rate", default=5e-5, type=float, help="The initial learning rate for Adam.")
        parser.add_argument("--max_training_steps", default=100000, type=int,
                            help="Total number of training steps; used to calculate the masking rate for 2 of the tasks")
        parser.add_argument("--model_name_or_path", default=None, type=str,
                            help="The model checkpoint for weights initialization. Leave None if you want to train a model from scratch.")
        parser.add_argument("--warmup_steps", default=0, type=int, help="Linear warmup over warmup_steps.")
        parser.add_argument("--weight_decay", default=0.0, type=float, help="Weight decay if we apply some.")
        # Arguments for mlm tasks
        parser.add_argument("--mlm_probability", type=float, default=0.3,
                            help="Ratio of tokens to mask for masked language modeling loss")
        parser.add_argument("--mlm_rdf", action="store_true", help="If true, apply mask language modeling on rdf")
        parser.add_argument("--mlm_text", action="store_true", help="If true, apply mask language modeling on text")
        parser.add_argument("--mlm_rdf_plus_text", action="store_true", help="If true, apply mask rdf, plus text, to rdf")
        parser.add_argument("--mlm_text_plus_rdf", action="store_true", help="If true, apply mask text, plus rdf, to text")
        parser.add_argument("--mlm_joint_to_rdf", action="store_true", help="If true, apply mask text, rdf, to rdf")
        parser.add_argument("--mlm_joint_to_text", action="store_true", help="If true, apply mask text, rdf, to text")
        parser.add_argument("--mlm_joint_to_joint", action="store_true", help="If true, apply mask text, rdf, to text & rdf")
        parser.add_argument("--graph2text", action="store_true", help="If true, train the model on the graph2text task")
        parser.add_argument("--text2graph", action="store_true", help="If true, train the model on the text2graph task")
        parser.add_argument("--span_masking", action="store_true", help="If true, replace a span of tokens with a single mask token; USED ONLY IF ONE OF mask_entities OR ONLY mask_properties IS TRUE")
        parser.add_argument("--entity_span_masking", action="store_true", help="If true, replace a span of tokens with a single mask token for entity masking; ONLY USED WHEN mask_entities & mask_properties ARE BOTH TRUE")
        parser.add_argument("--property_span_masking", action="store_true", help="If true, replace a span of tokens with a single mask token for property masking; ONLY USED WHEN mask_entities & mask_properties ARE BOTH TRUE")
        parser.add_argument("--mask_entities", action="store_true", help="If true, only mask ENTITIES in the pretraining tasks; If 'mask_entities' and 'mask_properties' are both false, random subtokens will be masked & if both are true, both will be masked in separate batches")
        parser.add_argument("--mask_properties", action="store_true", help="If true, only mask PROPERTIES in the pretraining tasks; If 'mask_entities' and 'mask_properties' are both false, random subtokens will be masked & if both are true, both will be masked in separate batches")
        parser.add_argument("--dynamic_augmented_masking", action="store_true", help="If true, use a dynamic masking rate for the text/rdf augmented masking tasks ([Masked Text + Graph -> Text] & [Text + Masked Graph -> Graph])")
        parser.add_argument("--dynamic_joint_masking", action="store_true", help="If true, use a dynamic masking rate for the text/rdf joint masking tasks ([Masked Text + Masked Graph -> Text] & [Masked Text + Masked Graph -> Graph])")
        parser.add_argument("--entity_masking_weight", type=float, help="Weight that the entity masking loss will have on the total loss")
        parser.add_argument("--property_masking_weight", type=float, help="Weight that the property masking loss will have on the total loss")
        parser.add_argument("--entity_lexicalization", action="store_true", help="Whether to train on a task where given an entity, the model is asked to predict the lexicalization, which comes from a corresponding text")
        parser.add_argument("--activate_adapter", action="store_true", help='Argument that you can define if you want to train an adapter rather than finetune a full model')
        parser.add_argument("--adapter_config", type=str, help="Adapter configuration identifier coming from the AdapterHub (https://docs.adapterhub.ml/overview.html)")
        parser.add_argument("--adapter_config_FT", type=str, help="Adapter configuration identifier used for finetuning, if you want to use a second adapter for finetuning to combine with the adapter trained during pretraining")
        parser.add_argument("--prefix_length", type=int, help="The length of the prefix when doing prefix finetuning")
        # Arguments for inference
        parser.add_argument("--predict_num_beams", default=5, type=int, 
                            help="Number of beams to use for beam search when generating predictions")
        parser.add_argument("--graph_max_gen_length", type=int, help="maximum length of a generated graph")
        parser.add_argument("--sent_max_gen_length", type=int, help="maximum length of a generated sentence")
        parser.add_argument("--experiment_name", type=str, help="Giving a name to your experiment that will be used as the directory name for all of the logs")

        return parent_parser
            
    def forward(self, input_ids, attention_mask, decoder_input_ids, labels, max_len):
        '''
        Method that is run during inference

        Parameters:
            input_ids (torch.Tensor): Tensor of input ids
            attention_mask (torch.Tensor): Tensor of attention masks
            dec_input (torch.Tensor): Tensor of decoder inputs
            labels (torch.Tensor): Tensor of labels
            max_len (int): Maximum length of the output sequence

        Returns:
            outputs (torch.Tensor): Tensor of generated text outputs from the model
        '''

        input_ids = input_ids.to(self.device)
        attention_mask = attention_mask.to(self.device)
        labels = labels.to(self.device)
        decoder_input_ids = decoder_input_ids.to(self.device)

        outputs = self.model.generate(
            input_ids,
            attention_mask=attention_mask,
            decoder_start_token_id=decoder_input_ids[0,0],
            num_beams=self.predict_num_beams,
            max_length=max_len
        )

        return outputs

    def configure_optimizers(self):
        '''
        Method to configure any optimizers and the learning rate schedulers

        Parameters:
            None (All of the parameters are passed to the class via argparse)

        Returns:
            optimizer (torch.optim.AdamW): AdamW optimizer
            If not finetuning:
                scheduler (torch.optim.lr_scheduler.LambdaLR): Learning rate scheduler
        '''
        if not self.finetune:
            model = self.model
            # Prepare optimizer and schedule (linear warmup and decay)
            no_decay = ["bias", "LayerNorm.weight"]
            optimizer_grouped_parameters = [
                {
                    "params": [
                        p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)
                    ],
                    "weight_decay": self.args.weight_decay,
                },
                {
                    "params": [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)],
                    "weight_decay": 0.0,
                },
            ]
            optimizer = AdamW(optimizer_grouped_parameters, lr=self.learning_rate, eps=self.args.adam_epsilon)

            scheduler = get_inverse_sqrt_schedule_with_warmup(
                optimizer, num_warmup_steps=self.args.warmup_steps, num_training_steps=self.args.max_training_steps)

            return [optimizer], [{"scheduler": scheduler, "interval": "step"}]
        else:
            optimizer = AdamW(self.model.parameters(), lr=self.learning_rate)
            return optimizer
        # "frequency": self.args.gradient_accumulation_steps <- deleted this from scheduler dictionary b/c I think scheduler automatically follows optimizer frequency

    def training_step(self, train_batch, batch_idx):
        '''
        Method to run a full training step on a batch of data

        Parameters:
            train_batch (torch.Tensor): Tensor of training data, which is organized as a dictionary
            batch_idx (int): Index of the batch

        Returns:
            loss (torch.Tensor): Loss value for the batch
        '''
        combined_loss = 0
        for fxn_idx, noise_fxn in enumerate(self.noise_functions):
            if self.args.mlm_text:  # [Masked Text + Empty Graph -> Text]
                # print(f'MLM TEXT: {self.global_step}')
                noise_fxn.mlm_probability = self.args.mlm_probability
                masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTEG2text(train_batch)
                if self.global_step == 0 and self.global_rank == 0:
                    save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_text_{fxn_idx}')

                # Any time we create a new tensor we need to move it to the correct device
                masked_input = masked_input.to(self.device)
                attention_mask = attention_mask.to(self.device)
                labels = labels.to(self.device)
                dec_input = dec_input.to(self.device)

                outputs = self.model(
                    input_ids=masked_input,
                    attention_mask=attention_mask,
                    decoder_input_ids=dec_input,
                    labels=labels
                )
                text_loss = outputs[0]  # model outputs are always tuple in transformers (see doc)
            else:
                text_loss = 0

            if self.args.mlm_rdf:  # [Empty Text + Masked Graph -> Graph]
                # print('MLM RDF')
                noise_fxn.mlm_probability = self.args.mlm_probability
                masked_input, attention_mask, dec_input, labels = noise_fxn.get_ETMG2graph(train_batch)
                if self.global_step == 0 and self.global_rank == 0:
                    save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_rdf_{fxn_idx}')

                masked_input = masked_input.to(self.device)
                attention_mask = attention_mask.to(self.device)
                labels = labels.to(self.device)
                dec_input = dec_input.to(self.device)

                outputs = self.model(
                    input_ids=masked_input,
                    attention_mask=attention_mask,
                    decoder_input_ids=dec_input,
                    labels=labels
                )
                rdf_loss = outputs[0]  # model outputs are always tuple in transformers (see doc)
            else:
                rdf_loss = 0

            if self.args.mlm_text_plus_rdf:  # [Masked Text + Graph -> Text]
                # print('MLM TEXT + RDF')
                if self.global_step % self.args.augmented_train_interval == 0:
                    if self.args.dynamic_augmented_masking:
                        mlm_probability = 0.1 + ((self.global_step / self.args.max_training_steps) * 0.75)
                        noise_fxn.mlm_probability = mlm_probability
                        # dynamic_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=mlm_probability)
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_PTPG2partial(train_batch, mask="text")
                    else:
                        noise_fxn.mlm_probability = self.args.mlm_probability
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_PTPG2partial(train_batch, mask="text")
                    if self.global_step == 0 and self.global_rank == 0:
                        save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_text_plus_rdf_{fxn_idx}')

                    masked_input = masked_input.to(self.device)
                    attention_mask = attention_mask.to(self.device)
                    labels = labels.to(self.device)
                    dec_input = dec_input.to(self.device)

                    outputs = self.model(
                        input_ids=masked_input,
                        attention_mask=attention_mask,
                        decoder_input_ids=dec_input,
                        labels=labels
                    )
                    text_augmented_loss = outputs[0]
                else:
                    text_augmented_loss = 0
            else:
                text_augmented_loss = 0

            if self.args.mlm_rdf_plus_text:  # [Text + Masked Graph -> Graph]
                # print('MLM RDF + TEXT')
                if self.global_step % self.args.augmented_train_interval == 0:
                    if self.args.dynamic_augmented_masking:
                        mlm_probability = 0.1 + ((self.global_step / self.args.max_training_steps) * 0.75)
                        noise_fxn.mlm_probability = mlm_probability
                        # dynamic_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=mlm_probability)
                        # print(f'DYNAMIC AUGMENTED MASKING RATE: {dynamic_mask.mlm_probability}')
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_PTPG2partial(train_batch, mask="rdf")
                    else:
                        noise_fxn.mlm_probability = self.args.mlm_probability
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_PTPG2partial(train_batch, mask="rdf")
                    if self.global_step == 0 and self.global_rank == 0:
                        save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_rdf_plus_text_{fxn_idx}')
                    
                    masked_input = masked_input.to(self.device)
                    attention_mask = attention_mask.to(self.device)
                    labels = labels.to(self.device)
                    dec_input = dec_input.to(self.device)

                    outputs = self.model(
                        input_ids=masked_input,
                        attention_mask=attention_mask,
                        decoder_input_ids=dec_input,
                        labels=labels
                    )
                    rdf_augmented_loss = outputs[0]
                else:
                    rdf_augmented_loss = 0
            else:
                rdf_augmented_loss = 0

            if self.args.mlm_joint_to_text:  # [Masked Text + Masked Graph -> Text]
                # print('MLM JOINT TEXT')
                if self.global_step % self.args.joint_train_interval == 0:
                    if self.args.dynamic_joint_masking:
                        mlm_probability = 0.1 + ((self.global_step / self.args.max_training_steps) * 0.75)
                        noise_fxn.mlm_probability = mlm_probability
                        # dynamic_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=mlm_probability)
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2partial(train_batch, pred="text")
                    else:
                        noise_fxn.mlm_probability = self.args.mlm_probability
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2partial(train_batch, pred="text")
                    if self.global_step == 0 and self.global_rank == 0:
                        save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_joint_to_text_{fxn_idx}')

                    masked_input = masked_input.to(self.device)
                    attention_mask = attention_mask.to(self.device)
                    labels = labels.to(self.device)
                    dec_input = dec_input.to(self.device)

                    outputs = self.model(
                        input_ids=masked_input,
                        attention_mask=attention_mask,
                        decoder_input_ids=dec_input,
                        labels=labels
                    )
                    text_joint_loss = outputs[0]
                else:
                    text_joint_loss = 0
            else:
                text_joint_loss = 0

            if self.args.mlm_joint_to_rdf:  # [Masked Text + Masked Graph -> Graph]
                # print('MLM JOINT RDF')
                if self.global_step % self.args.joint_train_interval == 0:
                    if self.args.dynamic_joint_masking:
                        mlm_probability = 0.1 + ((self.global_step / self.args.max_training_steps) * 0.75)
                        noise_fxn.mlm_probability = mlm_probability
                        # dynamic_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=mlm_probability)
                        # print(f'DYNAMIC JOINT MASKING RATE: {dynamic_mask.mlm_probability}')
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2partial(train_batch, pred="rdf")
                    else:
                        noise_fxn.mlm_probability = self.args.mlm_probability
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2partial(train_batch, pred="rdf")
                    if self.global_step == 0 and self.global_rank == 0:
                        save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_joint_to_rdf_{fxn_idx}')

                    masked_input = masked_input.to(self.device)
                    attention_mask = attention_mask.to(self.device)
                    labels = labels.to(self.device)
                    dec_input = dec_input.to(self.device)

                    outputs = self.model(
                        input_ids=masked_input,
                        attention_mask=attention_mask,
                        decoder_input_ids=dec_input,
                        labels=labels
                    )
                    rdf_joint_loss = outputs[0]
                else:
                    rdf_joint_loss = 0
            else:
                rdf_joint_loss = 0

            if self.args.mlm_joint_to_joint:  # [Masked Text + Masked Graph -> Text + Graph]
                noise_fxn.mlm_probability = self.args.mlm_probability
                masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2TG(train_batch)
                if self.global_step == 0 and self.global_rank == 0:
                    save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix='mlm_joint_to_joint')

                masked_input = masked_input.to(self.device)
                attention_mask = attention_mask.to(self.device)
                labels = labels.to(self.device)
                dec_input = dec_input.to(self.device)

                outputs = self.model(
                    input_ids=masked_input,
                    attention_mask=attention_mask,
                    decoder_input_ids=dec_input,
                    labels=labels
                )
                joint2joint_loss = outputs[0]
            else:
                joint2joint_loss = 0

            loss = (
                    text_loss
                    + rdf_loss
                    + text_augmented_loss
                    + rdf_augmented_loss
                    + text_joint_loss
                    + rdf_joint_loss
                    + joint2joint_loss
            )
            loss = loss * self.mask_weights[fxn_idx]
            combined_loss += loss

        if self.args.graph2text: # [Graph -> Text]
            gen_batch = get_generation(train_batch, self.tokenizer, g2t=True)
            save_dummy_batch(self.args, gen_batch['input_ids'], gen_batch['dec_input_ids'], gen_batch['labels'], self.tokenizer, prefix='g2t')

            input_ids = gen_batch['input_ids'].to(self.device)
            labels = gen_batch['labels'].to(self.device)
            src_attn_mask = gen_batch['src_attn_mask'].to(self.device)
            dec_input_ids = gen_batch['dec_input_ids'].to(self.device)

            outputs = self.model(
                input_ids=input_ids,
                attention_mask=src_attn_mask,
                decoder_input_ids=dec_input_ids,
                labels=labels
            )

            g2t_loss = outputs[0]
        else:
            g2t_loss = 0

        if self.args.text2graph: # [Text -> Graph]
            gen_batch = get_generation(train_batch, self.tokenizer, g2t=False)
            save_dummy_batch(self.args, gen_batch['input_ids'], gen_batch['dec_input_ids'], gen_batch['labels'], self.tokenizer, prefix='t2g')

            input_ids = gen_batch['input_ids'].to(self.device)
            labels = gen_batch['labels'].to(self.device)
            src_attn_mask = gen_batch['src_attn_mask'].to(self.device)
            dec_input_ids = gen_batch['dec_input_ids'].to(self.device)

            outputs = self.model(
                input_ids=input_ids,
                attention_mask=src_attn_mask,
                decoder_input_ids=dec_input_ids,
                labels=labels
            )

            t2g_loss = outputs[0]
        else:
            t2g_loss = 0

        if self.args.entity_lexicalization: # Entity -> Mention
            el_batch = get_entity_lex(train_batch, self.tokenizer)
            save_dummy_batch(self.args, el_batch['input_ids'], el_batch['dec_input_ids'], el_batch['labels'], self.tokenizer, prefix='EL')

            input_ids = el_batch['input_ids'].to(self.device)
            labels = el_batch['labels'].to(self.device)
            src_attn_mask = el_batch['src_attn_mask'].to(self.device)
            dec_input_ids = el_batch['dec_input_ids'].to(self.device)
            # print(self.tokenizer.decode(el_batch['input_ids'][0]))
            # print(self.tokenizer.decode(el_batch['dec_input_ids'][0]))

            outputs = self.model(
                input_ids=input_ids,
                attention_mask=src_attn_mask,
                decoder_input_ids=dec_input_ids,
                labels=labels
            )

            el_loss = outputs[0]
        else:
            el_loss = 0

        combined_loss = (
                    combined_loss
                    + g2t_loss
                    + t2g_loss
                    + el_loss
        )

        # if self.args.accumulate_grad_batches:
            # self.log('train_loss', loss, on_step=True, on_epoch=True, logger=True, sync_dist=True, batch_size = ((self.args.train_batch_size * self.args.accumulate_grad_batches) / self.args.devices))
        #else:
        self.log('train_loss', combined_loss, on_step=True, on_epoch=True, logger=True, sync_dist=True, batch_size = self.args.train_batch_size / self.args.devices)

        logs = {'train_loss': combined_loss}

        return {'loss': combined_loss, 'log': logs}

    def validation_step(self, val_batch, batch_idx):
        '''
        Method to run the validation step on a batch of data
            - essentially the same as the training step, especially since lightning handles backward pass and optimizers on its own

        Parameters:
            val_batch (torch.Tensor): Tensor of validation data, which is organized as a dictionary
            batch_idx (int): Index of the batch

        Returns:
            dict: Dictionary of validation loss and perplexity to be logged
        '''

        combined_loss = 0
        for fxn_idx, noise_fxn in enumerate(self.noise_functions):
            if self.args.mlm_text:  # [Masked Text + Empty Graph -> Text]
                # print(f'MLM TEXT: {self.global_step}')
                noise_fxn.mlm_probability = self.args.mlm_probability
                masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTEG2text(val_batch)
                if self.global_step == 0 and self.global_rank == 0:
                    save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_text_val_{fxn_idx}')

                # Any time we create a new tensor we need to move it to the correct device
                masked_input = masked_input.to(self.device)
                attention_mask = attention_mask.to(self.device)
                labels = labels.to(self.device)
                dec_input = dec_input.to(self.device)

                outputs = self.model(
                    input_ids=masked_input,
                    attention_mask=attention_mask,
                    decoder_input_ids=dec_input,
                    labels=labels
                )
                text_loss = outputs[0]  # model outputs are always tuple in transformers (see doc)
            else:
                text_loss = 0

            if self.args.mlm_rdf:  # [Empty Text + Masked Graph -> Graph]
                # print('MLM RDF')
                noise_fxn.mlm_probability = self.args.mlm_probability
                masked_input, attention_mask, dec_input, labels = noise_fxn.get_ETMG2graph(val_batch)
                if self.global_step == 0 and self.global_rank == 0:
                    save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_rdf_val_{fxn_idx}')

                masked_input = masked_input.to(self.device)
                attention_mask = attention_mask.to(self.device)
                labels = labels.to(self.device)
                dec_input = dec_input.to(self.device)

                outputs = self.model(
                    input_ids=masked_input,
                    attention_mask=attention_mask,
                    decoder_input_ids=dec_input,
                    labels=labels
                )
                rdf_loss = outputs[0]  # model outputs are always tuple in transformers (see doc)
            else:
                rdf_loss = 0

            if self.args.mlm_text_plus_rdf:  # [Masked Text + Graph -> Text]
                # print('MLM TEXT + RDF')
                if self.global_step % self.args.augmented_train_interval == 0:
                    if self.args.dynamic_augmented_masking:
                        mlm_probability = 0.1 + ((self.global_step / self.args.max_training_steps) * 0.75)
                        noise_fxn.mlm_probability = mlm_probability
                        # dynamic_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=mlm_probability)
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_PTPG2partial(val_batch, mask="text")
                    else:
                        noise_fxn.mlm_probability = self.args.mlm_probability
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_PTPG2partial(val_batch, mask="text")
                    if self.global_step == 0 and self.global_rank == 0:
                        save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_text_plus_rdf_val_{fxn_idx}')

                    masked_input = masked_input.to(self.device)
                    attention_mask = attention_mask.to(self.device)
                    labels = labels.to(self.device)
                    dec_input = dec_input.to(self.device)

                    outputs = self.model(
                        input_ids=masked_input,
                        attention_mask=attention_mask,
                        decoder_input_ids=dec_input,
                        labels=labels
                    )
                    text_augmented_loss = outputs[0]
                else:
                    text_augmented_loss = 0
            else:
                text_augmented_loss = 0

            if self.args.mlm_rdf_plus_text:  # [Text + Masked Graph -> Graph]
                # print('MLM RDF + TEXT')
                if self.global_step % self.args.augmented_train_interval == 0:
                    if self.args.dynamic_augmented_masking:
                        mlm_probability = 0.1 + ((self.global_step / self.args.max_training_steps) * 0.75)
                        noise_fxn.mlm_probability = mlm_probability
                        # dynamic_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=mlm_probability)
                        # print(f'DYNAMIC AUGMENTED MASKING RATE: {dynamic_mask.mlm_probability}')
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_PTPG2partial(val_batch, mask="rdf")
                    else:
                        noise_fxn.mlm_probability = self.args.mlm_probability
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_PTPG2partial(val_batch, mask="rdf")
                    if self.global_step == 0 and self.global_rank == 0:
                        save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_rdf_plus_text_val_{fxn_idx}')
                    
                    masked_input = masked_input.to(self.device)
                    attention_mask = attention_mask.to(self.device)
                    labels = labels.to(self.device)
                    dec_input = dec_input.to(self.device)

                    outputs = self.model(
                        input_ids=masked_input,
                        attention_mask=attention_mask,
                        decoder_input_ids=dec_input,
                        labels=labels
                    )
                    rdf_augmented_loss = outputs[0]
                else:
                    rdf_augmented_loss = 0
            else:
                rdf_augmented_loss = 0

            if self.args.mlm_joint_to_text:  # [Masked Text + Masked Graph -> Text]
                # print('MLM JOINT TEXT')
                if self.global_step % self.args.joint_train_interval == 0:
                    if self.args.dynamic_joint_masking:
                        mlm_probability = 0.1 + ((self.global_step / self.args.max_training_steps) * 0.75)
                        noise_fxn.mlm_probability = mlm_probability
                        # dynamic_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=mlm_probability)
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2partial(val_batch, pred="text")
                    else:
                        noise_fxn.mlm_probability = self.args.mlm_probability
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2partial(val_batch, pred="text")
                    if self.global_step == 0 and self.global_rank == 0:
                        save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_joint_to_text_val_{fxn_idx}')

                    masked_input = masked_input.to(self.device)
                    attention_mask = attention_mask.to(self.device)
                    labels = labels.to(self.device)
                    dec_input = dec_input.to(self.device)

                    outputs = self.model(
                        input_ids=masked_input,
                        attention_mask=attention_mask,
                        decoder_input_ids=dec_input,
                        labels=labels
                    )
                    text_joint_loss = outputs[0]
                else:
                    text_joint_loss = 0
            else:
                text_joint_loss = 0

            if self.args.mlm_joint_to_rdf:  # [Masked Text + Masked Graph -> Graph]
                # print('MLM JOINT RDF')
                if self.global_step % self.args.joint_train_interval == 0:
                    if self.args.dynamic_joint_masking:
                        mlm_probability = 0.1 + ((self.global_step / self.args.max_training_steps) * 0.75)
                        noise_fxn.mlm_probability = mlm_probability
                        # dynamic_mask = MaskMaster(args=self.args, tokenizer=self.tokenizer, mlm_probability=mlm_probability)
                        # print(f'DYNAMIC JOINT MASKING RATE: {dynamic_mask.mlm_probability}')
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2partial(val_batch, pred="rdf")
                    else:
                        noise_fxn.mlm_probability = self.args.mlm_probability
                        masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2partial(val_batch, pred="rdf")
                    if self.global_step == 0 and self.global_rank == 0:
                        save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix=f'mlm_joint_to_rdf_val_{fxn_idx}')

                    masked_input = masked_input.to(self.device)
                    attention_mask = attention_mask.to(self.device)
                    labels = labels.to(self.device)
                    dec_input = dec_input.to(self.device)

                    outputs = self.model(
                        input_ids=masked_input,
                        attention_mask=attention_mask,
                        decoder_input_ids=dec_input,
                        labels=labels
                    )
                    rdf_joint_loss = outputs[0]
                else:
                    rdf_joint_loss = 0
            else:
                rdf_joint_loss = 0

            if self.args.mlm_joint_to_joint:  # [Masked Text + Masked Graph -> Text + Graph]
                noise_fxn.mlm_probability = self.args.mlm_probability
                masked_input, attention_mask, dec_input, labels = noise_fxn.get_MTMG2TG(val_batch)
                if self.global_step == 0 and self.global_rank == 0:
                    save_dummy_batch(self.args, masked_input, dec_input, labels, self.tokenizer, prefix='mlm_joint_to_joint')

                masked_input = masked_input.to(self.device)
                attention_mask = attention_mask.to(self.device)
                labels = labels.to(self.device)
                dec_input = dec_input.to(self.device)

                outputs = self.model(
                    input_ids=masked_input,
                    attention_mask=attention_mask,
                    decoder_input_ids=dec_input,
                    labels=labels
                )
                joint2joint_loss = outputs[0]
            else:
                joint2joint_loss = 0

            loss = (
                    text_loss
                    + rdf_loss
                    + text_augmented_loss
                    + rdf_augmented_loss
                    + text_joint_loss
                    + rdf_joint_loss
                    + joint2joint_loss
            )
            loss = loss * self.mask_weights[fxn_idx]
            combined_loss += loss

        if self.args.graph2text: # [Graph -> Text]
            gen_batch = get_generation(val_batch, self.tokenizer, g2t=True)
            save_dummy_batch(self.args, gen_batch['input_ids'], gen_batch['dec_input_ids'], gen_batch['labels'], self.tokenizer, prefix='g2t_val')

            input_ids = gen_batch['input_ids'].to(self.device)
            labels = gen_batch['labels'].to(self.device)
            src_attn_mask = gen_batch['src_attn_mask'].to(self.device)
            dec_input_ids = gen_batch['dec_input_ids'].to(self.device)

            outputs = self.model(
                input_ids=input_ids,
                attention_mask=src_attn_mask,
                decoder_input_ids=dec_input_ids,
                labels=labels
            )

            g2t_loss = outputs[0]
        else:
            g2t_loss = 0

        if self.args.text2graph: # [Text -> Graph]
            gen_batch = get_generation(val_batch, self.tokenizer, g2t=False)
            save_dummy_batch(self.args, gen_batch['input_ids'], gen_batch['dec_input_ids'], gen_batch['labels'], self.tokenizer, prefix='t2g_val')

            input_ids = gen_batch['input_ids'].to(self.device)
            labels = gen_batch['labels'].to(self.device)
            src_attn_mask = gen_batch['src_attn_mask'].to(self.device)
            dec_input_ids = gen_batch['dec_input_ids'].to(self.device)

            outputs = self.model(
                input_ids=input_ids,
                attention_mask=src_attn_mask,
                decoder_input_ids=dec_input_ids,
                labels=labels
            )

            t2g_loss = outputs[0]
        else:
            t2g_loss = 0

        if self.args.entity_lexicalization: # Entity -> Mention
            el_batch = get_entity_lex(val_batch, self.tokenizer)
            save_dummy_batch(self.args, el_batch['input_ids'], el_batch['dec_input_ids'], el_batch['labels'], self.tokenizer, prefix='EL_val')

            input_ids = el_batch['input_ids'].to(self.device)
            labels = el_batch['labels'].to(self.device)
            src_attn_mask = el_batch['src_attn_mask'].to(self.device)
            dec_input_ids = el_batch['dec_input_ids'].to(self.device)

            outputs = self.model(
                input_ids=input_ids,
                attention_mask=src_attn_mask,
                decoder_input_ids=dec_input_ids,
                labels=labels
            )

            el_loss = outputs[0]
        else:
            el_loss = 0
            
        combined_loss = (
                    combined_loss
                    + g2t_loss
                    + t2g_loss
                    + el_loss
        )

        # perplexity = torch.exp(loss)

        self.log('val_loss', combined_loss, on_epoch=True, prog_bar=True, logger=True, sync_dist=True, batch_size = self.args.eval_batch_size / self.args.devices)
        # self.log('val_perplexity', perplexity, on_epoch=True, prog_bar=True, logger=True, sync_dist=True)

        # logs = {'val_loss': loss, 'val_perplexity': perplexity}
        logs = {'val_loss': combined_loss}
        # return {'loss': loss, 'val_perplexity': perplexity, 'log': logs}
        return {'loss': combined_loss, 'log': logs}
    
    def validation_epoch_end(self, output):

        if self.args.activate_adapter:
            loss = torch.stack([x["loss"] for x in output]).mean()

            if not self.min_val_loss:
                self.min_val_loss = loss
            else:
                if loss < self.min_val_loss:
                    self.min_val_loss = loss
                    save_path_adapter = f'/home/jkeenan/low_resource_rdf2text/lightning_logs/{self.args.experiment_name}/adapter_ckpt'
                    save_path_model = f'/home/jkeenan/low_resource_rdf2text/lightning_logs/{self.args.experiment_name}/model_ckpt'
                    print(f'New minimum validation loss... Saving model & adapter to {self.args.experiment_name}')
                    self.model.save_pretrained(save_path_model)
                    self.model.save_adapter(save_path_adapter, self.args.adapter_config)

            return self.min_val_loss
        else:
            pass

    def predict_step(self, pred_batch, batch_idx):
        '''
        Method to predict on a batch of data

        Parameters:
            pred_batch (dict): Batch of data to predict on
            batch_idx (int): Index of the batch

        Returns:
            predictions (dict): Dictionary of predictions
        
        I save the triples and sentences here since since the ordering of the outputs
        will not be the same as our reference files if using multiple GPUs.
        So these triples and sentences can be used to generate new reference files with the correct order.
        '''

        predictions = {}
        predictions['triples'] = pred_batch['input_ids']
        sentences = pred_batch['labels'].masked_fill_(pred_batch['labels'] == -100, self.tokenizer.pad_token_id)
        predictions['sentences'] = sentences

        # Set the max length for sentence generation
        if not self.args.sent_max_gen_length:
            self.args.sent_max_gen_length = max([len(s) for s in predictions['sentences']])
        # Set the max length for graph generation
        if not self.args.graph_max_gen_length:
            self.args.graph_max_gen_length = max([len(t) for t in predictions['triples']])

        if self.args.mlm_text:  # [Masked Text + Empty Graph -> Text]
            masked_input, attention_mask, dec_input, labels = self.mask.get_MTEG2text()

            outputs = self(input_ids=masked_input,
                           attention_mask=attention_mask,
                           decoder_input_ids=dec_input,
                           labels=labels,
                           max_len=self.args.sent_max_gen_length)

            predictions['mlm_text'] = outputs

        if self.args.mlm_rdf:  # [Empty Text + Masked Graph -> Graph]
            masked_input, attention_mask, dec_input, labels = self.mask.get_ETMG2graph(pred_batch)

            outputs = self(input_ids=masked_input,
                           attention_mask=attention_mask,
                           decoder_input_ids=dec_input,
                           labels=labels,
                           max_len=self.args.graph_max_gen_length)

            predictions['mlm_rdf'] = outputs

        if self.args.mlm_text_plus_rdf:  # [Masked Text + Graph -> Text]
            masked_input, attention_mask, dec_input, labels = self.mask.get_PTPG2partial(pred_batch, mask="text")

            outputs = self(input_ids=masked_input,
                           attention_mask=attention_mask,
                           decoder_input_ids=dec_input,
                           labels=labels,
                           max_len=self.args.sent_max_gen_length)

            predictions['mlm_text_plus_rdf'] = outputs

        if self.args.mlm_rdf_plus_text:  # [Text + Masked Graph -> Graph]
            masked_input, attention_mask, dec_input, labels = self.mask.get_PTPG2partial(pred_batch, mask="rdf")

            outputs = self(input_ids=masked_input,
                           attention_mask=attention_mask,
                           decoder_input_ids=dec_input,
                           labels=labels,
                           max_len=self.args.graph_max_gen_length)

            predictions['mlm_rdf_plus_text'] = outputs

        if self.args.mlm_joint_to_text:  # [Masked Text + Masked Graph -> Text]
            masked_input, attention_mask, dec_input, labels = self.mask.get_MTMG2partial(pred_batch, pred="text")

            outputs = self(input_ids=masked_input,
                           attention_mask=attention_mask,
                           decoder_input_ids=dec_input,
                           labels=labels,
                           max_len=self.args.sent_max_gen_length)

            predictions['mlm_joint_to_text'] = outputs

        if self.args.mlm_joint_to_rdf:  # [Masked Text + Masked Graph -> Graph]
            masked_input, attention_mask, dec_input, labels = self.mask.get_MTMG2partial(pred_batch, pred="rdf")

            outputs = self(input_ids=masked_input,
                           attention_mask=attention_mask,
                           decoder_input_ids=dec_input,
                           labels=labels,
                           max_len=self.args.graph_max_gen_length)

            predictions['mlm_joint_to_rdf'] = outputs

        if self.args.mlm_joint_to_joint:  # [Masked Text + Masked Graph -> Text + Graph]
            masked_input, attention_mask, dec_input, labels = self.mask.get_MTMG2TG(pred_batch)

            outputs = self(input_ids=masked_input,
                           attention_mask=attention_mask,
                           decoder_input_ids=dec_input,
                           labels=labels,
                           max_len=self.args.sent_max_gen_length + self.args.graph_max_gen_length)

            predictions['mlm_joint_to_joint'] = outputs

        if self.args.graph2text: # [Graph -> Text]
            gen_batch = get_generation(pred_batch, self.tokenizer, g2t=True)

            outputs = self(input_ids=gen_batch['input_ids'],
                           attention_mask=gen_batch['src_attn_mask'],
                           decoder_input_ids=gen_batch['dec_input_ids'],
                           labels=gen_batch['labels'],
                           max_len=self.args.sent_max_gen_length)

            predictions['graph2text'] = outputs

        if self.args.text2graph: # [Text -> Graph]
            gen_batch = get_generation(pred_batch, self.tokenizer, g2t=False)

            outputs = self(input_ids=gen_batch['input_ids'],
                           attention_mask=gen_batch['src_attn_mask'],
                           decoder_input_ids=gen_batch['dec_input_ids'],
                           labels=gen_batch['labels'],
                           max_len=self.args.graph_max_gen_length)

            predictions['text2graph'] = outputs

        # Decode all of the predictions
        for key in predictions.keys():
            predictions[key] = self.tokenizer.batch_decode(predictions[key], skip_special_tokens=True)

        return predictions


# Pytorch Lightning Data Module
class LitRDF2TextDataModule(pl.LightningDataModule):
    """
    Class to prepare data and split it into Dataloaders
    - Look into Pytorch Lightning 1.9.4 Documentation for more information:
        - https://lightning.ai/docs/stable/
    """

    def __init__(self, args, tokenizer, collate_fn, rank, world_size):
        super().__init__()
        self.args = args
        self.tokenizer = tokenizer  # tokenizer to use on the dataset
        self.collate_fn = collate_fn
        self.rank = rank
        self.world_size = world_size
        # Adapt batch sizes for distributed training
        self.train_batch_size = self.args.train_batch_size // self.world_size
        self.eval_batch_size = self.args.eval_batch_size // self.world_size
        

    @staticmethod
    def add_data_specific_args(parent_parser):
        parser = parent_parser.add_argument_group("LitDataModule")
        parser.add_argument("--data_path", default=None, type=str, required=True, help='Path to data')
        parser.add_argument("--train_batch_size", default=8, type=int, help="Batch size per GPU/CPU for training.")
        parser.add_argument("--eval_batch_size", default=16, type=int, help="Batch size per GPU/CPU for evaluation.")
        parser.add_argument("--buffer_size", default=1000, type=int, help="Number of samples to buffer for training data shuffling")

        return parent_parser
    
    def setup(self, stage):
        '''
        Method to prepare data to be passed to dataloaders
            - Specifically this creates a pytorch Dataset object for each split of the data
              and prepares the data for masking
        '''
        print(f'Preparing {stage} data...')
        if stage == 'fit' or stage == 'validation':
            self.dataset_train = CustomDataset(args=self.args,
                                                tokenizer=self.tokenizer,
                                                rank=self.rank,
                                                world_size=self.world_size,
                                                stage='train')
            self.dataset_train = ShuffleDataset(self.dataset_train, buffer_size=self.args.buffer_size)
            self.dataset_val = CustomDataset(args=self.args,
                                                tokenizer=self.tokenizer,
                                                rank=self.rank,
                                                world_size=self.world_size,
                                                stage='val')
        elif stage == 'test' or stage == 'predict':
            self.dataset_test = CustomDataset(args=self.args,
                                                tokenizer=self.tokenizer,
                                                rank=self.rank,
                                                world_size=self.world_size,
                                                stage='test')

    def train_dataloader(self):
        '''
        Method to create a dataloader for the training data
        *Note* --> Dataset was already shuffled when written into a file
                   since we cannot shuffle the IterableDataset,
                   this is why we do not shuffle the DataLoader here
        '''
        return DataLoader(self.dataset_train, batch_size=self.train_batch_size, collate_fn=self.collate_fn,
                          num_workers=os.cpu_count()//8, persistent_workers=True)
        # return DataLoader2(self.dataset_train, self.rs)

    def val_dataloader(self):
        '''
        Method to create a dataloader for the validation data
        '''
        return DataLoader(self.dataset_val, batch_size=self.eval_batch_size, collate_fn=self.collate_fn,
                          num_workers=os.cpu_count()//8, persistent_workers=True)
        # return DataLoader2(self.dataset_val, self.rs)

    def test_dataloader(self):
        '''
        Method to create a dataloader for the test data
        '''
        return DataLoader(self.dataset_test, batch_size=self.eval_batch_size, collate_fn=self.collate_fn)


class CustomWriter(BasePredictionWriter):
    # Class to write predictions from each GPU to a prediction file
    def __init__(self, output_dir, write_interval):
        super().__init__(write_interval)
        self.output_dir = output_dir

    def write_on_epoch_end(self, trainer, pl_module, predictions, batch_indices):
        torch.save(predictions, os.path.join(self.output_dir, 
                                             f"predictions_{trainer.global_rank}.pt"))
