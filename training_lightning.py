import os
from argparse import ArgumentParser
from transformers import AutoTokenizer 
import torch
from lightning_modules import LitRDF2TextBART, LitRDF2TextDataModule, CustomWriter
from data_interface.dataset_classes import DataCollatorForSeq2SeqWithMaskingAndPadding
from pytorch_lightning import Trainer
import pytorch_lightning.callbacks as cb
from pytorch_lightning.utilities import seed
from pytorch_lightning.tuner.tuning import Tuner
from pytorch_lightning.loggers import TensorBoardLogger
from utils.zeroto32_conversion import convert_deepspeed_checkpoint

# Command for easier debugging on multiple GPUs
CUDA_LAUNCH_BLOCKING = 1
# Command to fix problem with reading data from open files
torch.multiprocessing.set_sharing_strategy('file_system')
# Turning off parallelism within the tokenizer, since it is not possible when using the HF Fast Tokenizers
os.environ["TOKENIZERS_PARALLELISM"] = "false"

def main(args):
    '''
    Main function to run the lightning training
    '''

    # set the seed for reproducibility
    seed.seed_everything(seed=args.seed, workers=True)

    # Get number of GPUs on the machine
    if not args.devices:
        args.devices = torch.cuda.device_count()

    # Define the tokenizer
    if 'bart' in args.model_name_or_path:
        tok = AutoTokenizer.from_pretrained(args.model_name_or_path)
        new_special_tokens = {'additional_special_tokens': ['<g>', '</g>']}
        tok.add_special_tokens(new_special_tokens)
    elif 't5' in args.model_name_or_path:
        tok = AutoTokenizer.from_pretrained(args.model_name_or_path) 
        new_special_tokens = {'bos_token': '<s>', 'mask_token': '<mask>', 'additional_special_tokens': ['<g>', '</g>']}
        tok.add_special_tokens(new_special_tokens)


    # Define the train and eval batch sizes
    # args.train_batch_size = args.train_batch_size * max(1, n_gpu)
    # args.eval_batch_size = args.eval_batch_size * max(1, n_gpu)

    # Ensure that the max_length variable is set and correct
    if not args.max_length or args.max_length > tok.model_max_length:
        args.max_length = tok.model_max_length

    # Commented out b/c lightning instantiates the tensorboard logger by default & organizes everything better this way
    # Instantiate a Tensorboard Logger
    tb_logger = TensorBoardLogger(save_dir='lightning_logs', name=args.experiment_name)

    # Not using at the moment; may be more helpful when training model from scratch
    # Add a gradient accumulation of 2 iterations starting from epoch 0
    # grad_accumulation = cb.GradientAccumulationScheduler(scheduling={0: 2})


    # Instantiate all callbacks

    # Add early stopping if the validation loss does not improve for 25 iterations
    # monitor either val_loss or val_perplexity <- based off of your names for these variables in lightning_modules
    early_stop_callback = cb.EarlyStopping(monitor=args.stop_criteria, min_delta=0.00, patience=args.patience, verbose=False,
                                           mode='min')
    # Add a learning rate monitor
    lr_monitor = cb.LearningRateMonitor(logging_interval='step')

    # Add custom model checkpoints
    if args.stop_criteria == 'val_loss':
        filename='{epoch}-{val_loss:.3f}'
    elif args.stop_criteria == 'val_perplexity':
        filename='{epoch}-{val_perplexity:.3f}'
    checkpoint_callback = cb.ModelCheckpoint(save_top_k=1, monitor=args.stop_criteria, mode='min', filename=filename)

    # Add a writer to save the predictions to a file
    # output_dir = '/home/jkeenan/low_resource_rdf2text/predictions_pretraining/'
    # if not os.path.exists(output_dir):
        # os.mkdir(output_dir)
    # pred_writer = CustomWriter(output_dir=output_dir, write_interval='epoch')

    # Instantiate the trainer
    trainer = Trainer.from_argparse_args(args,
                                         logger=tb_logger,
                                         callbacks=[checkpoint_callback,
                                                    #grad_accumulation,
                                                    #pred_writer,
                                                    lr_monitor,
                                                    early_stop_callback,
                                                    cb.TQDMProgressBar()])
    
    rank = trainer.global_rank
    world_size = trainer.world_size

    # Instantiate the data collator and data module

    collate_fn = DataCollatorForSeq2SeqWithMaskingAndPadding(tokenizer=tok, max_length=args.max_length, padding=True)
    dataset = LitRDF2TextDataModule(args=args, tokenizer=tok, collate_fn=collate_fn, rank=rank, world_size=world_size)

    # Preprocess the data and organize it into dataloaders
    # dataset.prepare_data()
    dataset.setup('fit')
    data_train = dataset.train_dataloader()
    data_val = dataset.val_dataloader()

    if args.ckpt_path:
        # Instantiate the model from checkpoint
        print(f'Loading model from checkpoint {args.ckpt_path}')
        model = LitRDF2TextBART.load_from_checkpoint(args.ckpt_path, args=args, finetune=False)
    else:
        model = LitRDF2TextBART(args=args,
                                tokenizer=tok,
                                finetune=False)

    print(f'GLOBAL RANK CUSTOM: {trainer.global_rank}')

    # Train the model
    if args.auto_lr_find:
        tuner = Tuner(trainer)
        tuner.lr_find(model, datamodule=dataset, update_attr=True)
    if args.auto_scale_batch_size and args.devices == 1:
        tuner.scale_batch_size(model, mode='binsearch', datamodule=dataset, batch_arg_name='train_batch_size')

    trainer.fit(model, train_dataloaders=data_train, val_dataloaders=data_val)
    checkpoint_callback.best_model_path

    print('Converting Deepspeed checkpoint to fp32...')
    for root, dirs, files in os.walk(f'lightning_logs/{args.experiment_name}'):
        for dir in dirs:
            if dir.endswith(".ckpt"):
                final_ckpt = os.path.join(root, dir)

    pl_ckpt_path = convert_deepspeed_checkpoint(final_ckpt)
    print(f'Final checkpoint can be found in {pl_ckpt_path}')

if __name__ == "__main__":
    parser = ArgumentParser()

    #Add General arguments
    parser.add_argument(
        "--dataset",
        default=None,
        type=str,
        required=True,
        help='Name of the dataset being used; This is only used to add the dataset name to the output directory'
    )
    parser.add_argument(
        "--output_dir",
        type=str,
        required=False,
        help="The output directory where the model predictions and checkpoints will be written.",
    )
    parser.add_argument(
        "--max_length",
        default=1024,
        type=int,
        help="maximum length of input sequences"
    )
    parser.add_argument("--patience", 
        default=20,
        type=int,
        help="Number of validation epochs to keep training without improvement before early stopping")
    parser.add_argument(
        "--stop_criteria",
        type=str,
        help="Criteria used for saving checkpoints and early stopping in training"
    )
    parser.add_argument("--seed", type=int, default=42, help="random seed for initialization")

    # Add model specific arguments
    parser = LitRDF2TextBART.add_model_specific_args(parser)

    # Add data module specific arguments
    parser = LitRDF2TextDataModule.add_data_specific_args(parser)

    # Add all the available trainer options to argparse
    parser = Trainer.add_argparse_args(parser)

    # Parse all of the arguments
    args = parser.parse_args()

    # Run the main function
    main(args)
