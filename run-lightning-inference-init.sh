#!/bin/bash

source ~/.bashrc
conda activate rdf2text

cd /home/jkeenan/low_resource_rdf2text/

dataset=webnlg
datapath=/home/jkeenan/${dataset}_serialized
exp_name="webnlg_enriched_property_mask_span_pt18"

# model_size="large"
# MODEL="facebook/bart-${model_size}"
model_size="base"
MODEL="t5-$model_size"
ckpt="/home/jkeenan/low_resource_rdf2text/lightning_logs/webnlg_enriched_property_mask_span_pt18_FT/version_0/checkpoints/epoch=28-val_loss=0.476.pt"

python inference_lightning.py \
  --dataset $dataset \
  --data_path $datapath \
  --graph2text \
  --eval_batch_size 1 \
  --model_name_or_path $MODEL \
  --ckpt_path $ckpt \
  --sent_max_gen_length 200 \
  --predict_num_beams 5 \
  --accelerator gpu \
  --devices 1 \
  --experiment_name $exp_name \
  # Not currently using
    # --mlm_text_plus_rdf \
    # --mlm_rdf_plus_text \
    # --mlm_joint_to_text \
    # --mlm_joint_to_rdf \
    # --predict_num_beams 5 \
    # --output_dir $output_dir \
