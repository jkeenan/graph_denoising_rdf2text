import json
import re
from tqdm import tqdm
import pandas as pd
from datetime import datetime
import os
from argparse import ArgumentParser


def camel_case_split(identifier):
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    d = [m.group(0) for m in matches]
    new_d = []
    for token in d:
        token = token.replace('(', '')
        token_split = token.split('_')
        for t in token_split:
            new_d.append(t.lower())
    return new_d

def serialize_triples(triple_list):
    nodes = []
    curr_trips = {}
    for triple in triple_list:
        if len(triple) == 3:
            sub = ' '.join(triple[0].split('_'))
            pred = ' '.join(triple[1].split())
            pred = ' '.join(camel_case_split(pred))
            obj = ' '.join(triple[2].split('_'))
        else: # Ignore subject and qualifier, when a qualifier is included
            sub = ' '.join(triple[0].split('_'))
            pred = ' '.join(triple[2].split())
            pred = ' '.join(camel_case_split(pred))
            obj = ' '.join(triple[3].split('_'))
        if sub not in curr_trips.keys():
            curr_trips[sub] = []

        # Loop to add predicates and objects to a dictionary sorted by subjects
        if len(curr_trips[sub]) >= 1:
            curr_trips[sub][-1] = curr_trips[sub][-1] + ','
            curr_trips[sub].append(pred)
            curr_trips[sub].append(obj)
        else:
            curr_trips[sub].append(pred)
            curr_trips[sub].append(obj)

    # Loops to serialize the triples one subject at a time; so a subject will only ever be mentioned once per serialization
    for key in curr_trips:
        if len(curr_trips[key][-1]) >= 1:
            if curr_trips[key][-1][-1] == ',':
                curr_trips[key][-1] = curr_trips[key][-1][:-1]
    for idx, key in enumerate(curr_trips.keys()):
        nodes.append(key)
        for item in curr_trips[key]:
            nodes.append(item)
        if idx != len(curr_trips.keys()) - 1:
            nodes[-1] = nodes[-1] + ';'

    nodes = ' '.join(nodes)

    return nodes


def prepare_splits(data, split, savepath):

    if split == 'train':
        train_idx = int(len(data['text']) * 0.8)
        text = [x for x in data['text'][:train_idx]]
        triples = data['triples'][:train_idx]
    elif split == 'dev':
        train_idx = int(len(data['text']) * 0.8)
        test_idx = int(len(data['text']) * 0.9)
        text = [x for x in data['text'][train_idx:test_idx]]
        triples = data['triples'][train_idx:test_idx]
    elif split == 'test':
        test_idx = int(len(data['text']) * 0.9)
        text = [x for x in data['text'][test_idx:]]
        triples = data['triples'][test_idx:]

    final_data = []

    for idx, triple in enumerate(triples):
        nodes = serialize_triples(triple)
        final_data.append({'text': text[idx],
                            'triple': nodes})


    with open(f'{savepath}/{split}.source', 'w') as f:
        for inp in final_data:
            f.write(f'{inp["triple"]}\n')

    with open(f'{savepath}/{split}.target', 'w') as f:
        for inp in final_data:
            f.write(f'{inp["text"]}\n')

    print(f'{split} source and target data has been written to {savepath}')

    return final_data


def write_to_file(data, savepath, split):
    if split == 'valid':
        split = 'val'

    f1 = open(f'{savepath}/{split}.source', 'w')
    f2 = open(f'{savepath}/{split}.target', 'w')
    f3 = open(f'{savepath}/{split}.ents', 'w')
    if split != 'train':
        texts = [inp['text'] for inp in data]
        max_len = max([len(x) for x in texts])
        for idx, item in enumerate(texts):
            if len(texts[idx]) < max_len:
                while len(texts[idx]) < max_len:
                    texts[idx].append('')
        final_texts = []
        for x in range(max_len):
            temp = []
            for text in texts:
                temp.append(text[x])
            final_texts.append(temp)
            if x > 0:
                tmp = open(f'{savepath}/{split}.target{x+1}', 'w')
            else:
                tmp = open(f'{savepath}/{split}.target', 'w')
            tmp.write('\n'.join(final_texts[x]))
            tmp.write('\n')
            tmp.close()

    for inp in data:
        f1.write(f'{inp["triple"]}\n')
        if split == 'train':
            f2.write(f'{inp["text"]}\n')
        f3.write(f"{json.dumps(inp['mentions'])}\n")

    f1.close()
    f2.close()
    f3.close()

def get_time():
    '''
    Function to get the current time so you can track how long these preprocessing processes take
    '''
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")

    return current_time

def create_savepath(path):
    '''
    Function to create the save_path in the case that it doesn't exist
    '''
    if not os.path.exists(path):
        os.makedirs(path)


# Arguments for the script
parser = ArgumentParser()
parser.add_argument(
    '--datapath',
    type=str,
    required=True,
    help='Path to data that you want to serialize'
)
parser.add_argument(
    '--savepath',
    type=str,
    default=os.getcwd(),
    required=False,
    help='Path where you want to save your data'
)
args = parser.parse_args()

# Create the save_path if it doesn't exist
create_savepath(args.savepath)

# Read in the data
for file in os.listdir(args.datapath):
    if file.endswith(".json"):
        filename = os.path.splitext(file)[0]
        # f1 = open(f'{args.savepath}/{filename}.source', 'w', encoding='utf8')
        # f2 = open(f'{args.savepath}/{filename}.target', 'w', encoding='utf8')
        with open(f'{args.datapath}/{file}', encoding='utf8') as f:
            text = []
            triples = []
            ner2ent = []
            final_data = []
            f = json.load(f)
            for line in f:
                # line = json.loads(l)
                txt = line['target_txt'].split('@')
                txt = [x.strip() for x in txt]
                text.append(' '.join(txt))
                # triples.append(line['triples'])

                # Use the mtriple in the case that it contains all of the items
                # from the original triple, otherwise use the original triple
                # since sometimes the mtriple is missing information in the target text
                if len(line['triples']) == len(line['mtriples']):
                    triples.append(line['triples'])
                else:
                    triples.append(line['triples'])

                ner2ent_item = {' '.join(k.split('_')): v for k,v in line['mentions'].items()}
                ner2ent.append(ner2ent_item)
        added_triples = []
        for idx, triple in enumerate(triples):
            nodes = serialize_triples(triple)
            if 'test' in file or 'valid' in file:
                if triple in added_triples:
                    final_data[-1]['text'].append(text[idx])
                else:
                    final_data.append({'text': [text[idx]],
                                       'triple': nodes,
                                       'mentions': ner2ent[idx]
                                       })
            elif 'train' in file:
                final_data.append({'text': text[idx],
                                   'triple': nodes,
                                   'mentions': ner2ent[idx]
                                   })
            added_triples.append(triple)


        write_to_file(final_data, savepath=args.savepath, split=filename)

