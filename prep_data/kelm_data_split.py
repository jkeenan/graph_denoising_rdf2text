import os
import json
from tqdm import tqdm
import copy
import numpy as np
from argparse import ArgumentParser
from kelm_serialize import serialize_triples


class DataOrg:
    def __init__(self, args):
        self.args = args
        self.subset_filepath = self.args.subset_filepath
        self.save_path = self.args.save_path
        self.train_percent = self.args.train_percent
        # Dictionary giving the properties that appear in each triple length (Keys are triple lengths and values are lists of properties)
        self.length_prop_count = {}
        # Dictionary giving the number of appearances of a property per triple length (Keys are properties and values are dictionaries giving the triple lengths and count per length)
        self.prop_length_dist = {}
        # List that will contain all of the inputs read in from the subset file
        self.all_inputs = []
        # List of some chosen properties for the unseen test set; the get_triple_dist function can be used to find these properties with a good distribution among triple lengths
        self.unseen_props = ['acquisition transaction', 'league level above',
                            'has works in the collection', 'owner of']
        # Subset of items into batches of 20 items
        self.subset_batches = []
        # Final dictionary containing the data for each dataset split
        self.final_split_dict = {}
        # List to keep track of the properties used in the training split
        self.train_props = []
        # dictionary of the count and triple length distribution for each property
        # could be helpful for manually choosing some well-distributed and well represented properties for test_unseen
        self.property_stats = {}


    def get_triple_stats(self):
        # Getting statistics on the properties for each triple length

        with open(self.subset_filepath, 'r', encoding='utf8') as f:
            for index, line in tqdm(enumerate(f), desc='Getting stats on properties for each triple length'):
                line = json.loads(line)
                self.all_inputs.append(line)
                properties = line['main_properties']
                lengths = line['triple_len']
                for property_ in properties:
                    if lengths not in self.length_prop_count.keys():
                        self.length_prop_count[lengths] = []
                        self.length_prop_count[lengths].append(property_)
                    else:
                        self.length_prop_count[lengths].append(property_)
                    if property_ not in self.prop_length_dist.keys():
                        self.prop_length_dist[property_] = {k + 1: 0 for k in range(6)}
                        self.prop_length_dist[property_][lengths] = 1
                    else:
                        self.prop_length_dist[property_][lengths] += 1
            subset_length = index + 1

        print(f'{subset_length} inputs in the subset')

        return subset_length


    def get_triple_dist(self):
        '''
        Function to display the properties in order of the standard deviation of their triple
        length distribution
        - could be helpful to manually choose properties for the test unseen set
        '''
        # Organizing some further statistics on the distribution of each property among the triple lengths
        for key in self.prop_length_dist.keys():
            count_per_triple_length = [v for k, v in self.prop_length_dist[key].items()]
            total = sum(count_per_triple_length)
            percent_per_triple_length = []
            for k, v in self.prop_length_dist[key].items():
                percent_per_triple_length.append(v / total)
            self.property_stats[key] = {'property': key,
                                 'count': total,
                                 'standard deviation': np.std(percent_per_triple_length),
                                 'count distribution': count_per_triple_length,
                                 'percentage distribution': percent_per_triple_length,
                                        }
            self.property_stats = dict(sorted(self.property_stats.items(), key=lambda item: item[1]['standard deviation']))

        # for prop in triple_stats.keys():
            # print(prop)
            # for inner_key in triple_stats[prop].keys():
                # print(f'\t- {triple_stats[prop][inner_key]}')


    def organize_test_unseen(self):
        # List to collect 20 properties before being appended to all_subs
        batch = []
        # Organizing the batches of 20 inputs at a time
        for val in self.all_inputs:
            # for val in tqdm(prop_length_dist[key]):
            batch.append(val)
            if len(batch) == 20:
                self.subset_batches.append(batch)
                batch = []

        # Adding triples containing properties from our 'unseen' list into the test set
        found = 0
        for index, batch in enumerate(self.subset_batches):
            sub_copy = copy.deepcopy(batch)
            for i in batch:
                if set(i['main_properties']) & set(self.unseen_props):
                    found += 1
                    if 'test_both' not in self.final_split_dict.keys():
                        self.final_split_dict['test_both'] = []
                        self.final_split_dict['test_both'].append(i)
                    else:
                        self.final_split_dict['test_both'].append(i)
                    sub_copy.remove(i)
            self.subset_batches[index] = sub_copy


    def train_test_val_split(self, subset_length):
        # Organization of the rest of the inputs into train/val/test 20 items at a time
        """
        Here we add the first 90% of items (18/20 in the default case) to train and the
        next item is added to val, then if there is an item remaining is added to test
        -
        """
        test_val_size = int(subset_length * round(((1 - self.train_percent) / 2), 2))

        if 'train' not in self.final_split_dict.keys():
            self.final_split_dict['train'] = []
        if 'val' not in self.final_split_dict.keys():
            self.final_split_dict['val'] = []
        for sub in tqdm(self.subset_batches, desc='Creating main train/test/val splits'):
            if len(sub) == 19:
                self.final_split_dict['train'] += sub[:int(20*self.train_percent)]
                self.final_split_dict['val'].append(sub[int(20*self.train_percent)])
            else:
                split_val = int(self.train_percent * len(sub))
                self.final_split_dict['train'] += sub[:split_val]
                self.final_split_dict['val'].append(sub[split_val])
                if len(self.final_split_dict['test_both']) < test_val_size and split_val + 1 == len(sub) - 1:
                    self.final_split_dict['test_both'].append(sub[split_val + 1])
                elif len(self.final_split_dict['test_both']) >= test_val_size and split_val + 1 == len(sub) - 1:
                    self.final_split_dict['train'].append(sub[split_val + 1])

        for item in self.final_split_dict['train']:
            self.train_props += item['main_properties']
        self.train_props = set(self.train_props)


    def write_splits_to_files(self):

        if not os.path.exists(self.save_path):
            os.mkdir(self.save_path)

        test_seen = []
        test_unseen = []
        for item in self.final_split_dict['test_both']:
            if set(item['main_properties']) & set(self.unseen_props):
                test_unseen.append(item)
            elif set(item['main_properties']) & self.train_props:
                test_seen.append(item)
            else:
                test_unseen.append(item)

        self.final_split_dict['test_seen'] = test_seen
        self.final_split_dict['test_unseen'] = test_unseen

        splits = ['train', 'val', 'test_both', 'test_seen', 'test_unseen']
        for split in splits:
            if self.args.json:
                with open(f'{self.save_path}/{split}.jsonl', 'w+', encoding='utf8') as f:
                    for inp in self.final_split_dict[split]:
                        nodes, entities = serialize_triples(inp['triples'])
                        final_data = {
                            'text': inp['text'],
                            'triples': nodes,
                            'entities': entities
                        }
                        f.write(f'{json.dumps(final_data)}\n')
            else:
                final_data = []
                for inp in self.final_split_dict[split]:
                    nodes, entities = serialize_triples(inp['triples'])
                    final_data.append({
                        'text': inp['text'],
                        'triples': nodes,
                        'entities': entities
                    })
                with open(f'{self.save_path}/{split}.source', 'w', encoding='utf8') as f:
                    for inp in final_data:
                        f.write(f'{inp["triples"]}\n')

                with open(f'{self.save_path}/{split}.target', 'w', encoding='utf8') as f:
                    for inp in final_data:
                        f.write(f'{inp["text"]}\n')

                with open(f'{self.save_path}/{split}_ents.jsonl', 'w', encoding='utf8') as f:
                    for inp in final_data:
                        f.write(f'{json.dumps(inp["entities"])}\n')

        with open(f'{self.save_path}/prop_stats.jsonl', 'w', encoding='utf8') as f:
            for key in self.property_stats.keys():
                f.write(f'{json.dumps(self.property_stats[key])}\n')

        print(f'All dataset splits created and saved to {self.save_path}')


if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument(
        "--subset_filepath",
        type=str,
        required=True,
        help="Path to subset that was created via kelm_property_filter.py"
    )
    parser.add_argument(
        "--save_path",
        type=str,
        required=True,
        help="Path where you want to save the eventual serialized and split kelm subset"
    )
    parser.add_argument(
        "--train_percent",
        type=float,
        required=True,
        help="Percentage of the data you want in the train split; Remaining percent will be split between validation and test"
    )
    parser.add_argument(
        "--json",
        action="store_true",
        help="If True, all of the splits will be saved to their own json file; otherwise each split will be organized into source, target, and entity files"
    )


    args = parser.parse_args()
    data_org = DataOrg(args=args)

    subset_length = data_org.get_triple_stats()

    data_org.get_triple_dist()

    data_org.organize_test_unseen()

    data_org.train_test_val_split(subset_length)

    data_org.write_splits_to_files()