import pandas as pd
from datetime import datetime
import os
from argparse import ArgumentParser
import json

def serialize_triples(triple_list):
    nodes = []
    entities = []
    curr_trips = {}
    for triple in triple_list:
        if len(triple) == 3:
            sub = ' '.join(triple[0].split())
            pred = ' '.join(triple[1].split()).lower()
            obj = ' '.join(triple[2].split())
        else: # Ignore subject and qualifier, when a qualifier is included
            sub = ' '.join(triple[0].split())
            pred = ' '.join(triple[2].split()).lower()
            obj = ' '.join(triple[3].split())
        if sub not in curr_trips.keys():
            curr_trips[sub] = []

        # Loop to add predicates and objects to a dictionary sorted by subjects
        if len(curr_trips[sub]) >= 1:
            curr_trips[sub][-1] = curr_trips[sub][-1] + ','
            curr_trips[sub].append(pred)
            curr_trips[sub].append(obj)
        else:
            curr_trips[sub].append(pred)
            curr_trips[sub].append(obj)
        entities.append(sub)
        entities.append(obj)

    # Loops to serialize the triples one subject at a time; so a subject will only ever be mentioned once per serialization
    for key in curr_trips:
        if len(curr_trips[key][-1]) >= 1:
            if curr_trips[key][-1][-1] == ',':
                curr_trips[key][-1] = curr_trips[key][-1][:-1]
    for idx, key in enumerate(curr_trips.keys()):
        nodes.append(key)
        for item in curr_trips[key]:
            nodes.append(item)
        if idx != len(curr_trips.keys()) - 1:
            nodes[-1] = nodes[-1] + ';'

    nodes = ' '.join(nodes)
    entities = list(set(entities))
    return nodes, entities


def prepare_splits(data, split, savepath):
    '''
    if split == 'train':
        train_idx = int(len(data['text']) * 0.8)
        text = [x for x in data['text'][:train_idx]]
        triples = data['triples'][:train_idx]
    elif split == 'dev':
        train_idx = int(len(data['text']) * 0.8)
        test_idx = int(len(data['text']) * 0.9)
        text = [x for x in data['text'][train_idx:test_idx]]
        triples = data['triples'][train_idx:test_idx]
    elif split == 'test':
        test_idx = int(len(data['text']) * 0.9)
        text = [x for x in data['text'][test_idx:]]
        triples = data['triples'][test_idx:]
    '''
    final_data = []
    for idx, triple in enumerate(data['triples']):
        nodes, entities = serialize_triples(triple)
        final_data.append({'text': data['text'][idx],
                            'triple': nodes,
                            'entities': entities})


    with open(f'{savepath}/{split}.source', 'w', encoding='utf8') as f:
        for inp in final_data:
            f.write(f'{inp["triple"]}\n')

    with open(f'{savepath}/{split}.target', 'w', encoding='utf8') as f:
        for inp in final_data:
            f.write(f'{inp["text"]}\n')

    with open(f'{savepath}/{split}_ents.jsonl', 'w', encoding='utf8') as f:
        for inp in final_data:
            f.write(f"{json.dumps(inp)}\n")

    print(f'{split} source and target data has been written to {savepath}')

    return final_data


def write_to_file(data, savepath, split):

    with open(f'{savepath}/{split}.source', 'w', encoding='utf8') as f:
        for inp in data:
            f.write(f'{inp["triple"]}\n')

    with open(f'{savepath}/{split}.target', 'w', encoding='utf8') as f:
        for inp in data:
            f.write(f'{inp["text"]}\n')

def get_time():
    '''
    Function to get the current time so you can track how long these preprocessing processes take
    '''
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")

    return current_time

def create_savepath(path):
    '''
    Function to create the save_path in the case that it doesn't exist
    '''
    if not os.path.exists(path):
        os.makedirs(path)


# Arguments for the script
parser = ArgumentParser()
parser.add_argument(
    '--data_path',
    type=str,
    required=True,
    help='Path to data that you want to serialize'
)
parser.add_argument(
    '--save_path',
    type=str,
    default=os.getcwd(),
    required=False,
    help='Path where you want to save your data'
)
args = parser.parse_args()

# Create the save_path if it doesn't exist
create_savepath(args.save_path)

# Read in the data
current_time = get_time()
print(f'Started loading data at {current_time}')
kelm = pd.read_json(args.data_path, lines=True)
# kelm = kelm.sample(frac=1, random_state=14).reset_index(drop=True)
current_time = get_time()
print(f'Done loading data at {current_time}')

# Run the actual serialization
print('Train serialization...')
train_data = prepare_splits(kelm, split='train', savepath=args.save_path)


current_time = get_time()
print(f'Finished serializing data at {current_time}')
print(f'Data can be found in {args.save_path}')
