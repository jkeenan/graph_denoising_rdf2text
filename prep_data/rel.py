import requests
import os
import time
import random
import json
from tqdm import tqdm
from argparse import ArgumentParser
from concurrent.futures import as_completed
from requests_futures.sessions import FuturesSession


API_URL = "https://rel.cs.ru.nl/api"
# session = requests.Session()

def entity_finder(sents, graphs, ents):
    total_unfound = []
    used_sents = []
    total_ent_spans = []
    unfound_prop = []
    count = 0
    mention_not_found = 0
    entity_linker_found = 0
    for idx, (sent_item, graph_item, ent_list) in tqdm(enumerate(zip(sents, graphs, ents)), desc='Finding entity mention direct matches'):
        # print(sent_item)
        count += len(ent_list)
        ent_spans = {}
        found = [False for _ in ent_list]
        for ent in sorted(ent_list, key=len, reverse=True):
            ent_spans[ent] = {'graph_indicies': [],
                                  'mentions': [],
                                  'sent_indicies': []}

            # Run a loop to check for entities as many times as there are mentions
            # Usually an entity will only appear in a graph once, but sometimes an entity will appear as an object and later as a subject
            # This loop should account for those situations
            start = graph_item.find(ent)
            while start != -1:
                # if start != -1:
                end = start + len(ent)
                assert ent == graph_item[start:end]
                ent_spans[ent]['graph_indicies'].append((start, end - 1))
                graph_item = graph_item[:start] + 'X' * len(ent) + graph_item[end:]
                start = graph_item.find(ent)
        for idx2, ent in enumerate(sorted(ent_list, key=len, reverse=True)):
            start = sent_item.find(ent)
            while start != -1:
                found[idx2] = True
                # if start != -1:
                end = start + len(ent)
                assert ent == sent_item[start:end]
                ent_spans[ent]['mentions'].append(ent)
                ent_spans[ent]['sent_indicies'].append((start, end - 1))
                # print(sent_item)

                sent_item = sent_item[:start] + 'X' * len(ent) + sent_item[end:]
                start = sent_item.find(ent)
        used_sents.append(sent_item)
        total_ent_spans.append(ent_spans)
        if not all(found):
            unfound_entities = [item for idx, item in enumerate(ent_list) if found[idx] == False]
            total_unfound.append(unfound_entities)
            mention_not_found += len(unfound_entities)
        else:
            total_unfound.append(None)
        # mention_not_found += len(unfound_entities)
        # print(f'{mention_not_found/count} mentions not found')
        '''
        if idx != 0 and idx % 50 == 0:
            texts = [used_sents[idx] for idx in range(idx - 50, idx) if total_unfound[idx]]
            indicies = [idx for idx in range(idx - 50, idx) if total_unfound[idx]]
            print(texts)
            mentions_combined = run_entity_linker(texts)
            for mentions in mentions_combined:
                for result in mentions:
                    for idx in indicies:
                        if result[2] in entity:
                            entity_linker_found += 1
                            start = result[0]
                            length = result[1]
                            end = start + length
                            ent_spans[entity]['mentions'].append(entity)
                            ent_spans[entity]['sent_indicies'].append((start, end - 1))
                            unfound_entities.pop(idx)
                            # print(entity)
                            # print(sent_item[start:end], '\n')
                        
        '''
            # mention_not_found += len(unfound_entities)
        # total_ent_spans.append(ent_spans)
        # print(f'{mention_not_found} mentions not found')
        # print(f'{entity_linker_found} mentions found by entity linker')
        unfound_prop.append()
    print(f'{mention_not_found/count} mentions not found')
    return total_ent_spans


# Example EL.
def run_entity_linker(texts, try_number=1):
    # *** REPLACE API IN THIS FUNCTION ***
    el_results = []
    try:
        with FuturesSession() as session:
            futures = [session.post(API_URL, json={
                                    "text": text,
                                    "spans": []
                                    }) for text in texts]
        for future in tqdm(as_completed(futures)):
            response = future.result()
            el_results.append(response.json())
    except requests.JSONDecodeError:
        print('Encountered a requests json decode error... sleeping before next execution')
        time.sleep(2**try_number + random.random()*0.01) #exponential backoff
        return run_entity_linker(texts, try_number=try_number+1)
    else:
        return el_results


def main(args):

    # found_entities = {}
    for root, dirs, files in os.walk(args.data_path):
        for file in files:
            if file.endswith(".jsonl"):
                found_entities = []
                data_file = os.path.join(root, file)
                data_split = os.path.splitext(file)[0]
                graphs = []
                texts = []
                entities = []
                print(f'Processing {data_split} data...')
                with open(data_file,'r', encoding='utf-8') as f:
                    for line in f:
                        line = json.loads(line)
                        texts.append(line['text'].strip('\n'))
                        graphs.append(line['triple'].strip('\n'))
                        entities.append(line['entities'])
                '''
                if 'train' in data_split:
                    data_split2 = 'train'
                elif 'val' in data_split:
                    data_split2 = 'val'
                elif 'test' in data_split:
                    data_split2 = 'test_both'
                with open(os.path.join(root, f'{data_split2}.target'), 'r', encoding='utf8') as f:
                    for line in f:
                        print(line)
                        texts.append(line.strip('\n'))
                '''
                found_entities = entity_finder(sents=texts, graphs=graphs, ents=entities)

                with open(f'{args.data_path}/{data_split}_org.ents', 'w', encoding='utf8') as f:
                    for item in found_entities:
                        f.write(f'{json.dumps(item)}\n')
                print(f'Done writing new {data_split} ents file to {args.data_path}/{data_split}_org.ents...')

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        "--data_path",
        type=str,
        required=True,
        help="Path directory where kelm json files with text, triples, and entities are stored"
    )

    args = parser.parse_args()
    main(args=args)