import json
from tqdm import tqdm
import re
import copy
from argparse import ArgumentParser

# Version of kelm that has been filtered for graph/sent similarity, total graph length, and valid wikdiata properties
# filtered_kelm = '/Users/josephkeenan/nlp/alternance_2023/data_main/kelm_filter_steps/kelm_filter_step5.jsonl'

# subset_length = 300000
# subset_graph_length = subset_length / 6


class FilterFunctions:
    def __init__(self, args):
        self.args = args
        self.filtered_kelm = self.args.data_path
        self.subset_length = self.args.subset_length
        self.entered_threshold_condition = False
        self.max_graph_length = 1
        self.prop_count_by_length = {}
        self.prop_count_overall = {}
        self.subset = {}
        self.subset_prop_count = {}


    def get_data_stats(self):
        with open(self.filtered_kelm, 'r', encoding='utf-8') as f:
            for idx, line in tqdm(enumerate(f), desc='Getting stats on property distribution for each triple length'):
                line = json.loads(line)
                props = line['main_properties']
                lengths = line['triple_len']
                for prop in props:
                    if lengths not in self.prop_count_by_length.keys():
                        self.prop_count_by_length[lengths] = []
                        self.prop_count_by_length[lengths].append(prop)
                    else:
                        self.prop_count_by_length[lengths].append(prop)
                    if prop not in self.prop_count_overall .keys():
                        self.prop_count_overall [prop] = 1
                    else:
                        self.prop_count_overall [prop] += 1
            self.total_length = idx + 1

        # Get the percentage of occurrences for each property
        self.prop_count_overall = {k: v for k, v in self.prop_count_overall.items()}
        # Get the set of properties that occur for each graph length
        self.prop_count_by_length = {k: list(set(v)) for k, v in self.prop_count_by_length.items()}
        # Figure out actual max_graph_length
        self.max_graph_length = max(self.prop_count_by_length.keys())


    def run_filter(self, subset_threshold, overall_threshold):

        search_count = 0
        finished = [False] * self.max_graph_length
        subset_graph_length = int(round(self.subset_length / self.max_graph_length))
        added_inputs = [0 for _ in range(self.total_length)]
        incl_props = list(self.prop_count_overall.keys())
        property_proportion_check = list(self.subset_prop_count.keys())

        print(f'{len(incl_props)} total properties')

        while not all(finished):
            with open(self.filtered_kelm, 'r', encoding='utf8') as f:
                search_count += 1
                for idx, line in tqdm(enumerate(f), desc=f'Search {search_count} of subset splits'):
                    line = json.loads(line)
                    length = line['triple_len']
                    if length not in self.subset.keys():
                        self.subset[length] = []
                    found_prop = False
                    if len(self.subset[length]) < subset_graph_length and added_inputs[idx] != 1:
                        for prop in line['main_properties']:
                            # If we still have properties that are not included in our subset
                            if incl_props:
                                if prop in incl_props:
                                    found_prop = True
                                    break
                            else:
                                ''' 
                                If we have already included all of the properties, we
                                ensure that each property has reached the same proportion
                                found in the kelm dataset we started with
                                '''
                                if self.subset_prop_count[prop]/self.subset_length < self.prop_count_overall[prop]/self.total_length:
                                    found_prop = True
                                    break
                                elif prop in property_proportion_check and self.subset_prop_count[prop]/self.subset_length >= self.prop_count_overall[prop]/self.total_length:
                                    property_proportion_check.remove(prop)
                                elif not property_proportion_check:
                                    if self.subset_prop_count[prop] < subset_threshold and self.prop_count_overall[prop] > overall_threshold:
                                        '''
                                        If all of the property proportions have been met,
                                        we finish by adding properties with few occurrences
                                        '''
                                        self.entered_threshold_condition = True
                                        found_prop = True
                                        break

                        if found_prop is True:
                            self.subset[length].append(line)
                            added_inputs[idx] = 1
                            for prop in line['main_properties']:
                                if prop in incl_props:
                                    incl_props.remove(prop)
                                if prop not in self.subset_prop_count.keys():
                                    self.subset_prop_count[prop] = 1
                                else:
                                    self.subset_prop_count[prop] += 1

                    # If a certain graph length in our subset is complete
                    elif len(self.subset[length]) == subset_graph_length and finished[length - 1] is False:
                        finished[length - 1] = True
                        print(f'{length} search complete')
                        print(f'{len(incl_props)} properties remaining')

            print(f'----- Number of inputs per triple length after search {search_count} -----')
            for key in sorted(self.subset.keys()):
                print(f'{key}: {len(self.subset[key])}')
            print(f'{len(incl_props)} properties still to be added')
            print(self.entered_threshold_condition, subset_threshold, overall_threshold)
            if self.entered_threshold_condition:
                subset_threshold += 100
                overall_threshold += 200

        assert len([x for x in added_inputs if x == 1]) == subset_graph_length * self.max_graph_length


    def get_subset_stats(self):

        combined_stats = dict(sorted(self.subset_prop_count.items(), key=lambda x: x[1], reverse=True))
        combined_stats = {k: v for k, v in combined_stats.items()}
        combined_stats_rank = {key: idx + 1 for idx, key in enumerate(combined_stats.keys())}

        # Sort the property count in the similarity filter dataset and get the ranks of all of these items
        prop_count_overall = dict(sorted(self.prop_count_overall.items(), key=lambda x: x[1], reverse=True))
        prop_count_overall_rank = {key: idx + 1 for idx, key in enumerate(prop_count_overall.keys())}

        # Writing statistics about our data to a csv file (for comparison to similarity filter data)
        if self.subset_length < 100000:
            stats_save_path = f'{self.args.save_path}/filter_stats_{str(self.subset_length)[:2]}k.csv'
            subset_save_path = f'{self.args.save_path}/kelm_filter_{str(self.subset_length)[:2]}k.jsonl'
        if 100000 <= self.subset_length < 1000000:
            stats_save_path = f'{self.args.save_path}/filter_stats_{str(self.subset_length)[:3]}k.csv'
            subset_save_path = f'{self.args.save_path}/kelm_filter_{str(self.subset_length)[:3]}k.jsonl'
        elif self.subset_length > 1000000:
            stats_save_path = f'{self.args.save_path}/filter_stats_{str(self.subset_length)[:1]}m.csv'
            subset_save_path = f'{self.args.save_path}/kelm_filter_{str(self.subset_length)[:1]}m.jsonl'

        with open(stats_save_path, 'w+', encoding='utf8', ) as f:
            f.write('Property;Count in similarity filter;Count in final subset;'
                    'Prevalence Rank in similarity filter;Prevalence Rank in final subset;Difference in Rank;Percentage Proportion\n')
            for key in combined_stats.keys():
                f.write(f'{key};{prop_count_overall[key]};{combined_stats[key]};'
                        f'{prop_count_overall_rank[key]};{combined_stats_rank[key]};'
                        f'{prop_count_overall_rank[key] - combined_stats_rank[key]};'
                        f'{min(((prop_count_overall[key]/self.total_length)*100), ((combined_stats[key]/self.subset_length)*100)) / max(((prop_count_overall[key]/self.total_length)*100), ((combined_stats[key]/self.subset_length)*100))}\n')

        # Writing all of our data to a file
        with open(subset_save_path, 'w+', encoding='utf8') as f:
            for key in self.subset.keys():
                for item in self.subset[key]:
                    f.write(f'{json.dumps(item)}\n')

        print(f'Subset and Property statistics saved to {self.args.save_path}')

if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument(
        "--data_path",
        type=str,
        required=True,
        help="Path to larger kelm dataset from which you want to extract a subset"
    )
    parser.add_argument(
        "--save_path",
        type=str,
        required=True,
        help="Path where you want to save the eventual kelm subset"
    )
    parser.add_argument(
        "--subset_length",
        type=int,
        required=True,
        help="Size of the subset that you want to create"
    )
    parser.add_argument(
        "--subset_threshold",
        type=int,
        required=True,
        help="Property occurrence count threshold to decide if graph should be added after original proportions are met"
    )
    parser.add_argument(
        "--overall_threshold",
        type=int,
        required=True,
        help="Overall occurrence count threshold to decide if a graph should be added after original proportions are met"
    )


    args = parser.parse_args()

    main_filter = FilterFunctions(args)
    main_filter.get_data_stats()
    main_filter.run_filter(args.subset_threshold, args.overall_threshold)
    main_filter.get_subset_stats()