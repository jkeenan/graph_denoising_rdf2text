import re
import json
from tqdm import tqdm
from argparse import ArgumentParser


def test_entity_finder(sents, graphs, ents):
    total_ent_spans = []
    count = 0
    ent_not_found = 0
    mention_not_found = 0
    for sent_item, graph_item, ent_item in tqdm(zip(sents, graphs, ents)):
        count += 1
        ent_spans = {}
        for key in sorted(ent_item.keys(), key=len, reverse=True):
            if key != '' and key not in ent_spans.keys():
                ent_spans[key] = {'graph_indicies': [],
                                'mentions': [], 
                                'sent_indicies': []
                                }
            else:
                continue
            # Run a loop to check for entities as many times as there are mentions
            # Usually an enitiy will only appear in a graph once, but sometimes an entity will appear as an object and later as a subject
            # This loop should account for those situations
            start = graph_item.find(key)
            while start != -1:                
                # if start != -1:
                end = start + len(key)
                assert key == graph_item[start:end]
                ent_spans[key]['graph_indicies'].append((start, end - 1))
                graph_item = graph_item[:start] + 'X' * len(key) + graph_item[end:]
                start = graph_item.find(key)
                # else:
                    # if mention_count == 0:
                        # ent_not_found += 1
                        # print(key)
                        # print(graph_item, '\n')
        # for key, val in sorted(ent_item.items(), key=len, reverse=True):
        sorted_items = [(k, item) for k, v in ent_item.items() for item in v]
        sorted_items.sort(key = lambda x: len(x[1]), reverse = True)
        for key, val in sorted_items:
            if key != '':
                start = sent_item.find(val)
                while start != -1:
                # if start != -1:
                    end = start + len(val)
                    assert val == sent_item[start:end]
                    ent_spans[key]['mentions'].append(val)
                    ent_spans[key]['sent_indicies'].append((start, end - 1))
                    # print(sent_item)
                    sent_item = sent_item[:start] + 'X' * len(val) + sent_item[end:]
                    start = sent_item.find(val)
                    # print(sent_item)
                # else:
                    # mention_not_found += 1
                    # print(val)
                    # print(sorted_items)
                    # print(sent_item, '\n')

        total_ent_spans.append(ent_spans)
    # print(f'{ent_not_found} entities not in the graphs')
    # print(f'{mention_not_found} mentions not in the texts')
    
    return total_ent_spans


def main(args):
    # data_path = '/home/jkeenan/webnlg_enriched_serialized_tok'
    # write_to_file = False
    splits = ['train', 'val', 'test']
    sents = []
    graphs = []
    ents = []
    for split in splits:
        with open(f'{args.data_path}/{split}.target', 'r', encoding='utf8') as f:
            for line in f:
                sents.append(line.strip('\n'))

        with open(f'{args.data_path}/{split}.source', 'r', encoding='utf8') as f:
            for line in f:
                graphs.append(line.strip('\n'))

        with open(f'{args.data_path}/{split}.ents', 'r', encoding='utf8') as f:
            for line in f:
                # print(line)
                line = json.loads(line)
                ents.append(line)

    new_ents = test_entity_finder(sents, graphs, ents)

    if args.write_to_file:
        for split in splits:
            with open(f'{args.data_path}/{split}_org.ents', 'w', encoding='utf8') as f:
                for item in new_ents:
                    f.write(f'{json.dumps(item)}\n')
            print(f'Done writing new {split} ents file...')


if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument(
        "--data_path",
        type=str,
        required=True,
        help="Path to directory where enriched webnlg data is stored"
    )
    parser.add_argument(
        "--write_to_file",
        action='store_true',
        default=True,
        help="Whether you want to write the entity results to a file; they will be written to the data_path directory"
    )
    args = parser.parse_args()

    main(args)