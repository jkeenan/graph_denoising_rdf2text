from transformers import AutoTokenizer, AutoModel, BartForConditionalGeneration, BartTokenizer, utils
import torch
# from bertviz import head_view
from utils.general import mask_prep
from data_interface.dataset_classes import DataCollatorForSeq2SeqWithMaskingAndPadding
from lightning_modules import LitRDF2TextDataModule
from tqdm import tqdm
from utils.masking_functions import get_PTPG2partial
from argparse import ArgumentParser
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import seaborn as sns


def get_sent_graph_idxs(encoder_tokens):
    sent_indexes = []
    
    for idx, itm in enumerate(encoder_tokens):
        if itm != '<g>':
            sent_indexes.append(idx)
        else:
            break
    
    graph_len = len([idx for idx, item in enumerate(encoder_tokens) if idx > max(sent_indexes)])
    sent_len = len(sent_indexes)

    return sent_len, graph_len


def get_attention_heatmap(model_outputs, encoder_tokens, decoder_tokens, inp='text'):
    main_results = {}
    num_layers = len(model_outputs.cross_attentions)
    num_attn_heads = model_outputs.cross_attentions[0].shape[1]

    indicies = [index for index, x in enumerate(decoder_tokens)]

    for ind in indicies:
        
        # Dictionary which will have the combined attention scores for all layers and attention heads, separated by sentence and rdf attention, and organized by the masked token
        main_results[ind] = {} 

        # Running total of attention paid to the sentence
        total_attn = torch.zeros(len(encoder_tokens))

        for layer in range(num_layers):
            for attn_head in range(num_attn_heads):
                attn = model_outputs.cross_attentions[layer][:,attn_head, ind, :]
                # Add the attention scores for this attention head to a running score for all of the heads and layers
                total_attn = torch.add(total_attn, attn)

        main_results[ind] = total_attn.detach().numpy().reshape(39)
        
    main_results = {decoder_tokens[k]: v for k,v in main_results.items()}
    # main_results = {k: torch.numpy(v[0]) for k,v in main_results.items()}

    df = pd.DataFrame.from_dict(main_results, orient='index', columns=encoder_tokens)
    
    plt.figure()
    heatmap = sns.heatmap(df, linewidth=0.5, norm=LogNorm())
    # heatmap = heatmap.get_figure()
    plt.savefig(f'/home/jkeenan/low_resource_rdf2text/attention_plot_{inp}.png')
    print('Heatmap saved...')



def get_attention_scores(model_outputs, encoder_tokens, text_tokens, rdf_tokens, masked_indicies, sent_len, rdf_len, inp='sent'):
    main_results = {}
    max_attn_stats = {}
    num_layers = len(model_outputs.cross_attentions)
    num_attn_heads = model_outputs.cross_attentions[0].shape[1]

    if inp == 'rdf':
        masked_indicies = [ind - sent_len for ind in masked_indicies]

    for masked_ind in masked_indicies:
        
        # Dictionary which will have the combined attention scores for all layers and attention heads, separated by sentence and rdf attention, and organized by the masked token
        main_results[masked_ind] = {}
        # Dictionary which will have information about the tokens with the largest attention values, organized by the masked tokens
        max_attn_stats[masked_ind] = {} 

        # List which will hold the token with the largest attention score per layer
        layer_max = []
        # List which will hold the sentence token with the largest attention score per attention head
        sent_max_head = []
        # List which will hold the rdf token with the largest attention score per attention head
        rdf_max_head = []
        # Running total of attention paid to the sentence
        sent_attn = torch.zeros(sent_len)
        # Running total of attention paid to the rdf sequence
        rdf_attn = torch.zeros(rdf_len)
        for layer in range(num_layers):
            # List to hold the 
            sent_max_layer = []
            rdf_max_layer = []
            for attn_head in range(num_attn_heads):
                # Get cross attention scores between the predicted masked item in the decoder and the encoder input
                sent = model_outputs.cross_attentions[layer][:,attn_head, masked_ind, :sent_len]
                rdf = model_outputs.cross_attentions[layer][:,attn_head, masked_ind, sent_len:]
                # Add the attention scores for this attention head to a running score for all of the heads and layers
                sent_attn = torch.add(sent_attn, sent)
                rdf_attn = torch.add(rdf_attn, rdf)
                # Add the maximum score to a running list for the sentence and the rdf sequences
                sent_max_head.append(encoder_tokens[torch.argmax(sent)])
                rdf_max_head.append(rdf_tokens[torch.argmax(rdf)])

                # Get the maximum attention value for the cross attention fetched just above; for the sentence and the rdf sequence in the input
                if not sent_max_layer or torch.max(sent) > sent_max_layer[1]:
                    sent_max_layer = [encoder_tokens[torch.argmax(sent)], torch.max(sent)]
                if not rdf_max_layer or torch.max(rdf) > rdf_max_layer[1]:
                    rdf_max_layer = [rdf_tokens[torch.argmax(rdf)], torch.max(rdf)]
                
            # Append maximum attention value for the whole layer (all 16 attention heads) to a running list
            if inp == 'sent':
                layer_max.append(rdf_max_layer[0])
            elif inp == 'rdf':
                layer_max.append(sent_max_layer[0])

        main_results[masked_ind]['rdf'] = torch.sum(rdf_attn)
        main_results[masked_ind]['sent'] = torch.sum(sent_attn)

        
        if inp == 'sent':
            max_attn_stats[masked_ind]['rdf_layer_max'] = layer_max
            max_attn_stats[masked_ind]['rdf_head_max'] = rdf_max_head
            max_attn_stats[masked_ind]['mask_appearances'] = rdf_max_head.count(text_tokens[masked_ind])
        elif inp == 'rdf':
            max_attn_stats[masked_ind]['sent_layer_max'] = layer_max
            max_attn_stats[masked_ind]['sent_head_max'] = sent_max_head
            max_attn_stats[masked_ind]['mask_appearances'] = sent_max_head.count(rdf_tokens[masked_ind])
        
    return main_results, max_attn_stats


def main(batch, masked_input, decoder_input, model_outputs, tokenizer, inp, savepath):
    
    # Convert encoder and decoder ids back to tokens
    encoder_tokens = tokenizer.convert_ids_to_tokens(masked_input[0]) 
    decoder_tokens = tokenizer.convert_ids_to_tokens(decoder_input[0])

    # Get the indicies in the encoder tokens that have a masked token
    masked_indicies = [index for index, x in enumerate(encoder_tokens) if x == '<mask>']

    # Convert the input ids and labels back to tokens
    rdf_tokens = tokenizer.convert_ids_to_tokens(batch['input_ids'][0])
    text_tokens = batch['labels'][0].masked_fill_(batch['labels'][0] == -100, tokenizer.pad_token_id)
    text_tokens = tokenizer.convert_ids_to_tokens(text_tokens)

    # print(f'Encoder tokens: {encoder_tokens}')
    # print(f'Decoder tokens: {decoder_tokens}')
    # print(f'Graph: {inp_ids}')
    # print(f'Text: {labels}')
    # print(masked_indicies)
    '''
    html_head_view = head_view(encoder_attention=model_outputs.encoder_attentions, 
                            decoder_attention=model_outputs.decoder_attentions,
                            cross_attention=model_outputs.cross_attentions,
                            encoder_tokens=encoder_tokens,
                            decoder_tokens=decoder_tokens,
                            html_action='return')
    '''
    # with open(savepath, 'w') as file:
        # file.write(html_head_view.data)

    sent_ind, rdf_ind = get_sent_graph_idxs(encoder_tokens)
    results = get_attention_scores(model_outputs, encoder_tokens, text_tokens, rdf_tokens, masked_indicies, sent_ind, rdf_ind, inp=inp)
    results2 = get_attention_heatmap(model_outputs, encoder_tokens, decoder_tokens, inp=inp)

    return results


utils.logging.set_verbosity_error()  # Suppress standard warnings
tokenizer = BartTokenizer.from_pretrained("facebook/bart-large")
tokenizer.add_special_tokens({'additional_special_tokens': ['<g>', '</g>']})

parser = ArgumentParser()
parser = LitRDF2TextDataModule.add_data_specific_args(parser)
args = parser.parse_args()

state_dict = torch.load('/home/jkeenan/low_resource_rdf2text/lightning_logs/webnlg_pt20/checkpoints/epoch=36-val_loss=0.074_HF.pt')['state_dict']
model = BartForConditionalGeneration.from_pretrained('facebook/bart-large', state_dict=state_dict, vocab_size=len(tokenizer), 
                                                     output_attentions=True)
# model = AutoModel.from_pretrained("facebook/bart-large", output_attentions=True)


max_length = tokenizer.model_max_length
collate_fn = DataCollatorForSeq2SeqWithMaskingAndPadding(tokenizer=tokenizer, max_length=max_length, padding=True)
dataset = LitRDF2TextDataModule(args=args, tokenizer=tokenizer, collate_fn=collate_fn, rank=0, world_size=1)
dataset.setup('predict')
test_dataloader = dataset.test_dataloader()
sent_mask = {'sent': 0, 'rdf': 0}
rdf_mask = {'sent': 0, 'rdf': 0}
count = 0
sent_mask_count = 0
rdf_mask_count = 0
for idx, batch in tqdm(enumerate(test_dataloader)):
    if idx == 1:
        masked_input1, attention_mask1, dec_input1, labels1 = get_PTPG2partial(batch, tokenizer, inp='text', mlm_prob=0.3)
        outputs = model(input_ids=masked_input1,
                        attention_mask=attention_mask1,
                        decoder_input_ids=dec_input1,
                        labels=labels1)
        # print(masked_input1)
        results, attn_stats = main(batch=batch, masked_input=masked_input1, decoder_input=dec_input1,
                                model_outputs=outputs, tokenizer=tokenizer, inp='sent',
                                savepath="/home/jkeenan/low_resource_rdf2text/bertviz_results/head_view_2text.html")
        inp = tokenizer.convert_ids_to_tokens(masked_input1[0])
        print(' '.join(inp), '\n')
        print(results, '\n')
        print(attn_stats, '\n\n\n')
        for key in results.keys():
            sent_mask['sent'] += results[key]['sent']
            sent_mask['rdf'] += results[key]['rdf']
            sent_mask_count += 1
        # for key in results.keys():
            # val = tokenizer.convert_ids_to_tokens(batch['labels'][0])
            # print(f'- {val[key]}: {results[key]}\n{attn_stats[key]}\n\n')


        masked_input1, attention_mask1, dec_input1, labels1 = get_PTPG2partial(batch, tokenizer, inp='rdf', mlm_prob=0.3)
        outputs = model(input_ids=masked_input1,
                        attention_mask=attention_mask1,
                        decoder_input_ids=dec_input1,
                        labels=labels1)
        # print(masked_input1)
        results, attn_stats = main(batch=batch, masked_input=masked_input1, decoder_input=dec_input1,
                                model_outputs=outputs, tokenizer=tokenizer, inp='rdf',
                                savepath="/home/jkeenan/low_resource_rdf2text/bertviz_results/head_view_2text.html")
        inp = tokenizer.convert_ids_to_tokens(masked_input1[0])
        print(' '.join(inp), '\n')
        print(results, '\n')
        print(attn_stats)
        for key in results.keys():
            rdf_mask['sent'] += results[key]['sent']
            rdf_mask['rdf'] += results[key]['rdf']
            rdf_mask_count += 1
        count += 1

        if count % 100 == 0:
            print(f'----{count}----')
            print(f'-----> {sent_mask_count}')
            print(f'-----> {rdf_mask_count}')
            sent_mask_temp = {k:v/sent_mask_count for k,v in sent_mask.items()}
            rdf_mask_temp = {k:v/rdf_mask_count for k,v in rdf_mask.items()}
            print(f'SENT MASK: {sent_mask_temp}')
            print(f'RDF MASK: {rdf_mask_temp}')

print('---- FINAL SCORES ----')
sent_mask = {k:v/sent_mask_count for k,v in sent_mask.items()}
rdf_mask = {k:v/rdf_mask_count for k,v in rdf_mask.items()}
print(f'SENT MASK: {sent_mask}')
print(f'RDF MASK: {rdf_mask}')
    # for key in results.keys():
        # val = tokenizer.convert_ids_to_tokens(batch['input_ids'][0])
        # print(f'- {val[key]}: {results[key]}\n{attn_stats[key]}\n\n')