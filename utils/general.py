import json
import math
import os
import numpy as np
import torch
from torch import nn
from typing import Callable, Dict, Iterable, List, Tuple, Union, Any
from torch.optim import Optimizer
from numpy.lib.twodim_base import mask_indices
from torch.nn.utils.rnn import pad_sequence
from torch.distributed import init_process_group
from torch.optim.lr_scheduler import LambdaLR


def init_process(rank, num_processes):
    init_process_group(backend="gloo", init_method="tcp://localhost:1111", world_size=num_processes, rank=rank)

def grad_status(model: nn.Module) -> Iterable:
    return (par.requires_grad for par in model.parameters())


def lmap(f: Callable, x: Iterable) -> List:
    """list(map(f, x))"""
    return list(map(f, x))


def get_inverse_sqrt_schedule_with_warmup(
        optimizer: Optimizer, num_warmup_steps: int, num_training_steps: int, last_epoch: int = -1
):
    def lr_lambda(current_step):
        if current_step < num_warmup_steps:
            return float(current_step) / float(max(1, num_warmup_steps))
        return max(0.0, (num_warmup_steps / current_step) ** 0.5)

    return LambdaLR(optimizer, lr_lambda, last_epoch)


def ids_to_clean_text(tokenizer, generated_ids: List[int]):
    generated_ids.masked_fill_(generated_ids == -100, tokenizer.pad_token_id)
    gen_text = tokenizer.decode(generated_ids)

    return gen_text


def save_dummy_batch(args, input_ids, dec_inp_ids, labels, tokenizer, prefix="train"):

    id_file = f"lightning_logs/{args.experiment_name}/dummy_{prefix}_ids.json"
    token_file =  f"lightning_logs/{args.experiment_name}/dummy_{prefix}_tokens.json"

    if os.path.exists(id_file) is True and os.path.exists(token_file) is True:
        return

    dummy_ids, dummy_tokens = [], []
    for idx in range(len(input_ids)):
        ith_dict, ith_tok_dict = {}, {}
        ith_tok_dict["input_tokens"] = ids_to_clean_text(tokenizer, input_ids[idx])
        ith_tok_dict["label_tokens"] = ids_to_clean_text(tokenizer, labels[idx])
        ith_tok_dict["dec_inp_tokens"] = ids_to_clean_text(tokenizer, dec_inp_ids[idx])
        dummy_tokens.append(ith_tok_dict)

        # Convert the original tensors into lists so they can be written into the json file
        ith_dict["input_ids"] = input_ids[idx].tolist()
        ith_dict["label_ids"] = labels[idx].tolist()
        ith_dict["dec_inp_ids"] = dec_inp_ids[idx].tolist()
        dummy_ids.append(ith_dict)

    with open(id_file, "w", encoding="utf-8") as fout:
        json.dump(dummy_ids, fout, indent=4)
    with open(token_file, "w", encoding="utf-8") as fout:
        json.dump(dummy_tokens, fout, indent=4)


def padding_func(batch, padding_side="right", pad_token_id=1, key="label"):
    assert key in batch.keys(), f"{key} not in {batch.keys()}"
    max_label_length = max(len(itm) for itm in batch[key])
    for idx, itm in enumerate(batch[key]):
        remainder = [pad_token_id] * (max_label_length - len(itm))
        batch[key][idx] = (
            itm + remainder if padding_side == "right" else remainder + itm)


def mask_prep(data, tokenizer, max_length):
    '''
    Function to prepare the model inputs for the masked language model task
    into dictionary containing:
        - ids: which give the tokens of the text and/or graph in the inputs
        - seg_ids: which give the indexes of the text and/or graph in the inputs
    '''
    split_max_length = max_length // 2  # We divide max_length by two because this variable is used to truncate text and
    # graphs, so when they're combined for certain inputs their combined length (2x) cannot exceed this value
    model_inputs = {}
    bog_token_id = tokenizer.additional_special_tokens_ids[0]
    eog_token_id = tokenizer.additional_special_tokens_ids[1]

    # Creation of the sentence inputs: truncating sentences and adding bos & eos tokens
    model_inputs['labels'] = [
        [tokenizer.bos_token_id] + inp['input_ids'][1][:split_max_length - 2] + [tokenizer.eos_token_id] if len(inp['input_ids'][1]) > split_max_length - 2
    else [tokenizer.bos_token_id] + inp['input_ids'][1] + [tokenizer.eos_token_id] for inp in data]

    # Creation of the rdf inputs: truncating RDF graphs and adding graph bos & eos tokens
    model_inputs['input_ids'] = [
        [bog_token_id] + inp['input_ids'][0][:split_max_length - 2] + [eog_token_id]
        if len(inp['input_ids'][0]) > split_max_length - 2
        else [bog_token_id] + inp['input_ids'][0] + [eog_token_id]
    for inp in data]

    # Creation of joint ids when generating sentences: tokens for mlm tasks where we have a sentence and a graph
    joint_ids = [sent + trip for sent, trip in zip(model_inputs['labels'], model_inputs['input_ids'])]
    joint_ids = [itm[:max_length - 1] + [eog_token_id] if len(itm) > max_length else itm
        for itm in joint_ids]
    model_inputs[
        'joint_ids'] = joint_ids  # [<s> x1,x2...,xn </s> <g> y1,y2,...ym </g>] <- Tokens for a combined text and graph

    # Creation of joint ids when generating graphs: tokens for mlm tasks where we have a sentence and a graph
    # joint_ids_2graph = [trip + sent for sent, trip in zip(model_inputs['labels'], model_inputs['input_ids'])]
    # joint_ids_2graph = [itm[:max_length - 1] + [tokenizer.eos_token_id] if len(itm) > max_length else itm
        # for itm in joint_ids_2graph]
    # model_inputs[
        # 'joint_ids_2graph'] = joint_ids_2graph  # [<g> x1,x2...,xn </g> <s> y1,y2,...ym </s>] <- Tokens for a combined text and graph

    # Creation of seg ids when generating a sentence: indexes the sentence and the graph for the mlm tasks where we have a sentence and a graph
    
    seg_ids = [[0 for _ in range(len(sent))] + [1 for _ in range(len(trip))]
            for sent, trip in zip(model_inputs['labels'], model_inputs[
            'input_ids'])]  # Id's to give indexes of text and graph in combined input
    seg_ids = [itm[:max_length] for itm in seg_ids]
    model_inputs['seg_ids'] = seg_ids
    
    # Creation of seg ids when generating a graph: indexes the sentence and the graph for the mlm tasks where we have a sentence and a graph
    # seg_ids_2graph = [[1 for _ in range(len(trip))] + [0 for _ in range(len(sent))]
            # for sent, trip in zip(model_inputs['labels'], model_inputs[
            # 'input_ids'])]  # Id's to give indexes of text and graph in combined input
    # seg_ids_2graph = [itm[:max_length] for itm in seg_ids_2graph]
    # model_inputs['seg_ids_2graph'] = seg_ids_2graph
    
    # Creation of sentErdf_ids: tokens for the mlm tasks where we have a sentence and the graph is empty
    sentErdf_ids = [sent[:max_length - 4] + [tokenizer.eos_token_id,
                                            bog_token_id,
                                            tokenizer.mask_token_id,
                                            eog_token_id]
                    if len(sent) > max_length - 4 else sent + [
                                            bog_token_id,
                                            tokenizer.mask_token_id,
                                            eog_token_id]
                    for sent in
                    model_inputs['labels']]  # [<s> x1,x2...,xn <\s> <g> [mask] </g>] -> sentence with an empty graph

    # Creation of Esentrdf_ids: tokens for the mlm tasks where we have a graph and the sentence is empty
    rdfEsent_ids = [rdfi[:max_length - 4] + [eog_token_id,
                                            tokenizer.bos_token_id,
                                            tokenizer.mask_token_id,
                                            tokenizer.eos_token_id]
                    if len(rdfi) > max_length - 4 else rdfi + [
                                            tokenizer.bos_token_id,
                                            tokenizer.mask_token_id,
                                            tokenizer.eos_token_id]
                    for rdfi in
                    model_inputs['input_ids']]  # [<s> x1,x2...,xn <\s> <g> [mask] </g>] -> sentence with an empty graph

    # Creations of sentErdf_segids and Esentrdf_segids: indexes the sentence and the graph for the mlm tasks where we only have a sentence or only have a graph
    
    sentErdf_segids = [[0 for _ in range(len(itm) - 3)] + [1 for _ in range(3)] for itm in
                    sentErdf_ids]  # [0,0,...,0,1,1,...1] -> sentence with an empty RDF graph segment ids
    rdfEsent_segids = [[1 for _ in range(len(itm) - 3)] + [0 for _ in range(3)] for itm in
                    rdfEsent_ids]  # [0,0,...,0,1,1,...1] -> Empty sentence with an RDF graph segment ids
    
    model_inputs['sentErdf_ids'] = sentErdf_ids  # Input tokens with a text and an empty graph
    model_inputs['rdfEsent_ids'] = rdfEsent_ids  # Input tokens with an empty text and a graph
    model_inputs['sentErdf_segids'] = sentErdf_segids  # Ids to give indexes of text and empty graph
    model_inputs['rdfEsent_segids'] = rdfEsent_segids  # Ids to give indexes of empty graph and text

    return model_inputs

