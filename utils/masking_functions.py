import torch
from torch.nn.utils.rnn import pad_sequence
from numpy.random import poisson, uniform


class MaskMaster:
    def __init__(self, args, tokenizer, mlm_probability, mask_entities, mask_properties, span_masking):
        self.args = args
        self.mlm_probability = mlm_probability
        self.tokenizer = tokenizer
        self.mask_entities = mask_entities
        self.mask_properties = mask_properties
        self.span_masking = span_masking

    def random_masking(self, input_tensor):
        """
        Main masking function (which performs the actual masking)
            - All other functions below rely on this function

        Function to mask mlm_probability % (plan on using 30%) of the input tokens one token at a time

        Parameters:
            - input_tensor: the input_tensor

        Outputs:
            - masked_input: the input_tensor with the masked tokens
        """

        input_length = len([itm != self.tokenizer.pad_token_id for itm in input_tensor])
        # Number of tokens that will be masked in input (according to mlm_probability)
        masking_length = round(input_length * self.mlm_probability)
        masked_length = 0
        # Creating copy of the input tensor
        masked_input = input_tensor.clone().tolist()
        # Creating list of masked indicies so we can be sure to always mask an unmasked token
        masked_indicies = torch.zeros_like(input_tensor).tolist()
        while masked_length < masking_length:

            if self.span_masking:
                poisson_val = 3
                # span length will be the minimum of the poisson value and the input length - 2 (to account for the start and end tokens)
                span_length = min(int(poisson(poisson_val, 1)), input_length - 2)
                # start index will be a random integer between 1 and the input length - 1 - span_length (to account for the start and end tokens as well as span length)
                start_index = int(uniform(1, (input_length - 1) - span_length, 1))
                if 1 not in masked_indicies[start_index:start_index + span_length]:
                    masked_indicies[start_index:start_index + span_length] = [1] * (
                                (start_index + span_length) - start_index)
                    masked_input = masked_input[:start_index] + [self.tokenizer.mask_token_id] + masked_input[start_index + span_length:]
                    masked_length += span_length
            else:
                span_length = 1
                start_index = int(uniform(1, (input_length - 1) - span_length, 1))
                if masked_indicies[start_index] != 1:
                    masked_indicies[start_index] = 1
                    masked_input = masked_input[:start_index] + [self.tokenizer.mask_token_id] + masked_input[start_index + span_length:]
                    masked_length += span_length

        masked_input = torch.LongTensor(masked_input)

        return masked_input


    def entity_masking(self, input_tensor, entities):
        """
        Masking function to mask either entities or properties in an input
            - All other functions below rely on this function

        Function to mask mlm_probability % (plan on using 30%) of the input tokens one token at a time

        Parameters:
            - input_tensor: the input_tensor
            - mask_indicies: list of indicies of each entity in the input

        Outputs:
            - masked_input: the input_tensor with the masked tokens
        """

        input_length = len([itm != self.tokenizer.pad_token_id for itm in input_tensor])
        # Number of tokens that will be masked in input (according to mlm_probability)
        masking_length = round(input_length * self.mlm_probability)
        masked_length = 0
        # Creating copy of the input tensor
        masked_input = input_tensor.clone().tolist()
        if type(entities) != list:
            entities = entities.clone().tolist()
        mask_sequences = []
        smallest_sequence = None

        while masked_length < masking_length and entities:
            mask_index = int(uniform(0, (len(entities) - 1), 1))
            mask_sequence = entities[mask_index]
            if smallest_sequence is None:
                smallest_sequence = mask_sequence
            else:
                if mask_sequence[1] - mask_sequence[0] < smallest_sequence[1] - smallest_sequence[0]:
                    smallest_sequence = mask_sequence
            entity_length = ((mask_sequence[1] + 1) - mask_sequence[0])
            if masked_length + entity_length <= masking_length:
                mask_sequence = [seq + 1 for seq in mask_sequence] # Add 1 to indicies to account for <bos> tokens
                mask_sequences.append(mask_sequence)
                masked_length += entity_length
                # start_index = mask_sequence[0] + 1 # Add +1 to start_index to account for bos token
                # end_index = mask_sequence[1] + 1 # Add +1 to end index to account for bos token
            entities.pop(mask_index)

        if not entities and masked_length == 0:
            # If all of the possible entities in the list have brought our masking_length above the 30% threshold, and thus have left the entities list empty, just mask the smallest entity in the list
            # This is to avoid instances where we do not mask any items
            mask_sequence = [seq + 1 for seq in smallest_sequence] # Add 1 to indicies to account for <bos> tokens
            # entity_length = ((mask_sequence[1] + 1) - mask_sequence[0])
            mask_sequences.append(mask_sequence)
            # masked_length += entity_length


        rolling_indicies = 0
        mask_sequences.sort(key = lambda x: x[0])
        for seq in mask_sequences:
            start_index = seq[0] - rolling_indicies
            end_index = seq[1] - rolling_indicies
            if self.span_masking:
                rolling_indicies += (end_index - start_index)
                masked_input = masked_input[:start_index] + [self.tokenizer.mask_token_id] + masked_input[end_index + 1:]
                # masked_length += entity_length
            else:
                masked_input = masked_input[:start_index] + [self.tokenizer.mask_token_id for _ in range(start_index, end_index + 1)] + masked_input[end_index + 1:]
                # masked_length += entity_length


        masked_input = torch.LongTensor(masked_input)

        return masked_input
    

    def property_masking(self, input_tensor, entities):

        properties = self.get_property_indicies(entities)

        masked_input = self.entity_masking(input_tensor, properties)

        return masked_input


    def joint_infilling_partial(self, inp, seg_id, mask_txt=True, entity_ids=None):
        """
        Function to mask either the text OR the RDF graphs in a given input
        """
        res = []
        if entity_ids:
            for inp_ids, seg_iid, entity_id in zip(inp, seg_id, entity_ids):
                inp_ids = torch.LongTensor([iid for iid in inp_ids if iid != self.tokenizer.pad_token_id])
                text_ids = torch.LongTensor([inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 0])
                rdf_ids = torch.LongTensor([inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 1])
                entity_ids = torch.LongTensor(entity_id)
                if mask_txt == True:
                    if self.mask_entities:
                        masked_text = self.entity_masking(text_ids, entity_ids)
                    elif self.mask_properties:
                        masked_text = self.property_masking(text_ids, entity_ids)
                    res.append(torch.cat([masked_text, rdf_ids], dim=0))
                else:
                    if self.mask_entities:
                        masked_rdf = self.entity_masking(rdf_ids, entity_ids)
                    elif self.mask_properties:
                        masked_rdf = self.property_masking(rdf_ids, entity_ids)
                    res.append(torch.cat([text_ids, masked_rdf], dim=0))

        else:
            for inp_ids, seg_iid in zip(inp, seg_id):
                inp_ids = torch.LongTensor([iid for iid in inp_ids if iid != self.tokenizer.pad_token_id])
                text_ids = torch.LongTensor([inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 0])
                rdf_ids = torch.LongTensor([inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 1])
                if mask_txt:
                    masked_text = self.random_masking(text_ids)
                    res.append(torch.cat([masked_text, rdf_ids], dim=0))
                else:
                    masked_rdf = self.random_masking(rdf_ids)
                    # res.append(torch.cat([masked_rdf, text_ids], dim=0))
                    # Original ordering of text and graph (with text always first and graph always second)
                    res.append(torch.cat([text_ids, masked_rdf], dim=0))

        return pad_sequence(res, batch_first=True, padding_value=self.tokenizer.pad_token_id)


    def joint_infilling_full(self, inp, seg_id, pred_text=True, 
                            sent_ids=None, graph_ids=None):
        """
        Function to mask the text AND RDF graphs in a given input
        """
        res = []
        if sent_ids and graph_ids:
            for inp_ids, seg_iid, sent_id, graph_id in zip(inp, seg_id, sent_ids, graph_ids):
                inp_ids = torch.LongTensor([iid for iid in inp_ids if iid != self.tokenizer.pad_token_id])
                text_ids = torch.LongTensor([inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 0])
                rdf_ids = torch.LongTensor([inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 1])
                sent_indicies = torch.LongTensor(sent_id)
                graph_indicies = torch.LongTensor(graph_id)
                if self.mask_entities:
                    masked_text = self.entity_masking(text_ids, sent_indicies)
                    masked_rdf = self.entity_masking(rdf_ids, graph_indicies)
                elif self.mask_properties:
                    masked_text = self.property_masking(text_ids, sent_indicies)
                    masked_rdf = self.property_masking(rdf_ids, graph_indicies)
                if pred_text:
                    res.append(torch.cat([masked_text, masked_rdf], dim=0))
                else:
                    # res.append(torch.cat([masked_rdf, masked_text], dim=0))
                    # Original ordering of text and graph (with text always first and graph always second)
                    res.append(torch.cat([masked_text, masked_rdf], dim=0))

        else:
            for inp_ids, seg_iid in zip(inp, seg_id):
                inp_ids = torch.LongTensor([iid for iid in inp_ids if iid != self.tokenizer.pad_token_id])
                text_ids = torch.LongTensor([inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 0])
                rdf_ids = torch.LongTensor([inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 1])
                masked_text = self.random_masking(text_ids)
                masked_rdf = self.random_masking(rdf_ids)
                if pred_text:
                    res.append(torch.cat([masked_text, masked_rdf], dim=0))
                else:
                    # res.append(torch.cat([masked_rdf, masked_text], dim=0))
                    # Original ordering of text and graph (with text always first and graph always second)
                    res.append(torch.cat([masked_text, masked_rdf], dim=0))
                
        return pad_sequence(res, batch_first=True, padding_value=self.tokenizer.pad_token_id)


    def get_MTEG2text(self, batch):
        """
        [Masked Text + Empty Graph -> Text]

        ** Note: I've only added comments to this function to explain the process
        because it is essentially the same for all of the other functions below **
            - The only difference will the the values as each function is used for a different mlm task
        """
        # Get the correct input and segment ids from our dictionary that was created in the dataset_classes.py script
        ori_input = batch["sentErdf_ids"]
        seg_ids = batch["sentErdf_segids"]
        if self.mask_entities or self.mask_properties:
            entities = batch['sent_indicies']
            # Pass the masked input to the joint_infilling_partial function to mask the text
            masked_input = self.joint_infilling_partial(ori_input, seg_ids, mask_txt=True, entity_ids=entities)
        else:
            # Pass the masked input to the joint_infilling_partial function to mask the text
            masked_input = self.joint_infilling_partial(ori_input, seg_ids, mask_txt=True)

        # Add attention mask to all values that are not padding id
        attention_mask = masked_input.ne(self.tokenizer.pad_token_id).int()              # attention mask
        # Create the labels by cloning the sentence
        labels = batch["labels"].clone()                                         # <s> x1...,xn </s> pad pad
        # Remove the first token from the labels since the first token is fed to the decoder and not predicted
        labels = labels[:, 1:]                                                      # x1...,xn </s> pad pad
        # Create the decoder input by cloning the sentence
        dec_input = batch["labels"].clone()
        # Remove the last token from the decoder input since the last token is not fed to the decoder
        dec_input = dec_input[:, :-1]
        # Make sure that if we have -100 tokens in the decoder input that they are converted to the padding token
        dec_input.masked_fill_(dec_input == -100, self.tokenizer.pad_token_id)
        return masked_input, attention_mask, dec_input, labels


    def get_ETMG2graph(self, batch):
        """
        [Empty text + Masked Graph -> Graph]
        """
        ori_input = batch["rdfEsent_ids"]
        seg_ids = batch["rdfEsent_segids"]
        if self.mask_entities or self.mask_properties:
            entities = batch['graph_indicies']
            masked_input = self.joint_infilling_partial(ori_input, seg_ids, mask_txt=False, entity_ids=entities)
        else:
            masked_input = self.joint_infilling_partial(ori_input, seg_ids, mask_txt=False)
            
        attention_mask = masked_input.ne(self.tokenizer.pad_token_id).int()              # attention mask
        labels = batch["input_ids"].clone()
        labels.masked_fill_(labels == self.tokenizer.pad_token_id, -100)
        labels = labels[:, 1:]                                                      # x1...,xn </g> pad pad
        dec_input = batch["input_ids"].clone()
        dec_input = dec_input[:, :-1]
        return masked_input, attention_mask, dec_input, labels


    def get_PTPG2partial(self, batch, mask='text'):
        """
        If inp == text, then [Masked text + Graph -> Text]
        If inp != text, then [Text + Masked Graph -> Graph]
        """
        
        if mask == 'text':
            ori_input = batch["joint_ids"]
            seg_ids = batch["seg_ids"]
            if self.mask_entities or self.mask_properties:
                entities = batch['sent_indicies']
                masked_input = self.joint_infilling_partial(ori_input, seg_ids, mask_txt=True, entity_ids=entities)
            else:
                masked_input = self.joint_infilling_partial(ori_input, seg_ids, mask_txt=True)

            labels = batch["labels"].clone()
            labels = labels[:, 1:]                                                  # x1, x2, .., xn </s>
            dec_input = batch["labels"].clone()
            dec_input = dec_input[:, :-1]                                           # <s> x1, x2, ..., xn
            dec_input.masked_fill_(dec_input == -100, self.tokenizer.pad_token_id)

        else:
            ori_input = batch["joint_ids"]
            seg_ids = batch["seg_ids"]
            if self.mask_entities or self.mask_properties:
                entities = batch['graph_indicies']
                masked_input = self.joint_infilling_partial(ori_input, seg_ids, mask_txt=False, entity_ids=entities)
            else:
                masked_input = self.joint_infilling_partial(ori_input, seg_ids, mask_txt=False)
                
            labels = batch["input_ids"].clone()
            labels.masked_fill_(labels == self.tokenizer.pad_token_id, -100)
            labels = labels[:, 1:]                                                  # x1...,xn </g> pad pad
            dec_input = batch["input_ids"].clone()
            dec_input = dec_input[:, :-1]

        attention_mask = masked_input.ne(self.tokenizer.pad_token_id).int()              # attention mask
        return masked_input, attention_mask, dec_input, labels


    def get_MTMG2partial(self, batch, pred='text'):
        """
        If inp == text, then [Masked Text + Masked Graph -> Text]
        If inp != text, then [Masked Text + Masked Graph -> Graph]
        """
        
        if pred == 'text':
            ori_input = batch["joint_ids"]
            seg_ids = batch["seg_ids"]
            if self.mask_entities or self.mask_properties:
                sent_entities = batch['sent_indicies']
                graph_entities = batch['graph_indicies']
                masked_input = self.joint_infilling_full(ori_input, seg_ids, pred_text=True, sent_ids=sent_entities, graph_ids=graph_entities)
            else:
                masked_input = self.joint_infilling_full(ori_input, seg_ids, pred_text=True)
                
            labels = batch["labels"].clone()
            labels = labels[:, 1:]                                                  # x1, x2, .., xn </s>
            dec_input = batch["labels"].clone()
            dec_input = dec_input[:, :-1]
            dec_input.masked_fill_(dec_input == -100, self.tokenizer.pad_token_id)       # <s> x1 x2, ..., xn

        else:
            ori_input = batch["joint_ids"]
            seg_ids = batch["seg_ids"]
            if self.mask_entities or self.mask_properties:
                sent_entities = batch['sent_indicies']
                graph_entities = batch['graph_indicies']
                masked_input = self.joint_infilling_full(ori_input, seg_ids, pred_text=False, sent_ids=sent_entities, 
                                                    graph_ids=graph_entities)
            else:
                masked_input = self.joint_infilling_full(ori_input, seg_ids, pred_text=False)
                
            labels = batch["input_ids"].clone()
            labels.masked_fill_(labels == self.tokenizer.pad_token_id, -100)
            labels = labels[:, 1:]                                                  # x1...,xn </g> pad pad
            dec_input = batch["input_ids"].clone()
            dec_input = dec_input[:, :-1]

        attention_mask = masked_input.ne(self.tokenizer.pad_token_id).int()              # attention mask
        return masked_input, attention_mask, dec_input, labels


    def get_MTMG2TG(self, batch):
        """
        [Masked Text + Masked Graph -> Text + Graph]
        """
        ori_input = batch["joint_ids"]
        seg_ids = batch["seg_ids"]
        if self.mask_entities or self.mask_properties:
            sent_entities = batch['sent_indicies']
            graph_entities = batch['graph_indicies']
            masked_input = self.joint_infilling_full(ori_input, seg_ids, pred_text=True,
                                                sent_ids=sent_entities, graph_ids=graph_entities)
        else:
            masked_input = self.joint_infilling_full(ori_input, seg_ids, pred_text=True)

        labels = batch["joint_ids"].clone()
        labels.masked_fill_(labels == self.tokenizer.pad_token_id, -100)
        labels = labels[:, 1:]                                                      # x1, x2, .., xn </g>
        dec_input = batch["joint_ids"].clone()
        dec_input = dec_input[:, :-1]                                               # <s> x1 x2, ..., xn
        attention_mask = masked_input.ne(self.tokenizer.pad_token_id).int()              # attention mask
        return masked_input, attention_mask, dec_input, labels
    

    def get_property_indicies(self, entities):
        # sort mask indicies by the order they appear in the input
        entities = entities.clone().tolist()

        properties = []
        entities.sort(key = lambda x: x[0])
        final_mask = entities[-1]
        for idx, pair in enumerate(entities):
            if pair != final_mask:
                prop_start = pair[1] + 1
                prop_end = entities[idx + 1][0] - 1
                properties.append((prop_start, prop_end))
            else:
                continue

        return properties



def get_generation(batch, tokenizer, g2t=True):
    """
    Function for graph -> text and text -> graph generation
    if g2t=True, then graph -> text
    if g2t=False, then text -> graph
    """
    bog_token_id = tokenizer.additional_special_tokens_ids[0]
    eog_token_id = tokenizer.additional_special_tokens_ids[1]

    if g2t:
        # If we are doing graph -> text
        input_ids = []
        labels = batch['labels']

        for inp_ids, seg_iid in zip(batch['joint_ids'],batch["seg_ids"]):
            inp_ids = [iid for iid in inp_ids if iid != tokenizer.pad_token_id]
            rdf_ids = [inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 1]
            # rdf_ids = torch.LongTensor(rdf_ids + [tokenizer.bos_token_id, tokenizer.eos_token_id])
            # Original ordering with text always preceding the graph
            rdf_ids = torch.LongTensor([tokenizer.bos_token_id, tokenizer.eos_token_id] + rdf_ids)
            input_ids.append(rdf_ids)
        input_ids = pad_sequence(input_ids, batch_first=True, padding_value=tokenizer.pad_token_id)
    else:
        # If we are doing text -> graph
        input_ids = []
        labels = batch['input_ids']
        labels.masked_fill_(labels == tokenizer.pad_token_id, -100)

        for inp_ids, seg_iid in zip(batch['joint_ids'], batch["seg_ids"]):
            inp_ids = [iid for iid in inp_ids if iid != -100]
            text_ids = [inp_ids[idx] for idx in range(len(inp_ids)) if seg_iid[idx] == 0]
            text_ids = torch.LongTensor(text_ids + [bog_token_id,
                                                    eog_token_id])
            input_ids.append(text_ids)
        input_ids = pad_sequence(input_ids, batch_first=True, padding_value=tokenizer.pad_token_id)


    # Create attention masks
    batch['src_attn_mask'] = input_ids.ne(tokenizer.pad_token_id).int()
    # Create the decoder input
    dec_input = labels.clone()
    dec_input = dec_input[:, :-1]
    dec_input.masked_fill_(dec_input == -100, tokenizer.pad_token_id)
    batch['dec_input_ids'] = dec_input

    # Get rid of first ID in the labels
    labels = labels[:, 1:]

    return {
        "input_ids": input_ids,
        "labels": labels,
        "dec_input_ids": batch["dec_input_ids"],
        "src_attn_mask": batch['src_attn_mask']
    }


def get_entity_lex(batch, tokenizer):

    input_ids = batch['input_ids']
    labels = batch['labels']
    
    dec_input = labels.clone()
    dec_input = dec_input[:, :-1]
    dec_input.masked_fill_(dec_input == -100, tokenizer.pad_token_id)
    src_attention_mask = input_ids.ne(tokenizer.pad_token_id).int()

    input_ids = pad_sequence(input_ids, batch_first=True, padding_value=tokenizer.pad_token_id)
    labels = labels[:,1:]
    
    return {
        "input_ids": input_ids,
        "labels": labels,
        "dec_input_ids": dec_input,
        "src_attn_mask": src_attention_mask
    }