import os
import pickle
import torch
from argparse import ArgumentParser


def find_pred_files(pred_path, pred_ext):
    pred_files = []
    for root, dirs, files in os.walk(pred_path):
        for file in files:
            if file.endswith(pred_ext):
                pred_files.append(os.path.join(root, file))
    return pred_files


def organize_preds(pred_files, save_path):
    results = {}
    for file in pred_files:
        predictions = torch.load(file)
        for batch in predictions:
            for inner in batch:
                for key in inner.keys():
                    if key not in results:
                        results[key] = []
                    for item in inner[key]:
                        results[key].append(item)

    for key in results.keys():
        with open(f'{save_path}/{key}.txt', 'w') as f:
            print(f'Writing {save_path}/{key}.txt')
            for item in results[key]:
                f.write(f'{item}\n')


def main(args):

    if not os.path.exists(args.pred_path):
        return print(f'Exiting script... Path {args.pred_path} does not exist')
        
    pred_files = find_pred_files(args.pred_path, args.pred_extension)
    organize_preds(pred_files, args.save_path)
    print(f'Processing complete. You can find the organized files in {args.save_path}')


if __name__ == "__main__":
    parser = ArgumentParser()

    # Possible arguments
    parser.add_argument(
        "--pred_path",
        default=None,
        type=str,
        required=True,
        help='Path to directory where your prediction files are stored'
    )
    parser.add_argument(
        '--pred_extension',
        default='pt',
        type=str,
        required=False,
        help='File extension of the prediction files (make sure this file extension is unique to the prediction files in the directory)'
    )
    parser.add_argument(
        '--save_path',
        default=None,
        type=str,
        required=False,
        help='Path where you would like the final files to be stored'
    )

    args = parser.parse_args()
    if args.save_path is None:
        args.save_path = args.pred_path
    main(args)
