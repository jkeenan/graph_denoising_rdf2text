'''
FUNCTIONS TO CONVERT DEEPSPEED SHARDED CHECKPOINTS INTO ONE SINGLE PYTORCH CHECKPOINT
- Used this script below which was found here: https://github.com/Lightning-AI/lightning/issues/15694
- Essentially just adds missing keys/parameters to the DeepSpeed checkpoint
'''


import os
from argparse import ArgumentParser
import torch
from pytorch_lightning.utilities.deepspeed import (
        convert_zero_checkpoint_to_fp32_state_dict,
        get_model_state_file,
        get_optim_files,
        ds_checkpoint_dir
)
import re

DS_PARAM_REGEX = r'_forward_module\.(.+)'

def convert_deepspeed_checkpoint(deepspeed_ckpt_path: str, pl_ckpt_path: str = None):
    '''
    Creates a PyTorch Lightning checkpoint from the DeepSpeed checkpoint directory, while patching
    in parameters which are improperly loaded by the DeepSpeed conversion utility.
    deepspeed_ckpt_path: Path to the DeepSpeed checkpoint folder.
    pl_ckpt_path: Path to the reconstructed PyTorch Lightning checkpoint. If not specified, will be
        placed in the same directory as the DeepSpeed checkpoint directory with the same name but
        a .pt extension.
    Returns: path to the converted checkpoint.
    '''
    if not (deepspeed_ckpt_path.endswith('.ckpt') and os.path.isdir(deepspeed_ckpt_path)):
        raise ValueError(
            'args.ckpt_path should point to the checkpoint directory'
            ' output by DeepSpeed (e.g. "last.ckpt" or "epoch=4-step=39150.ckpt").'
        )

    # Convert state dict to PyTorch format
    if not pl_ckpt_path:
        pl_ckpt_path = f'{deepspeed_ckpt_path[:-4]}pt' # .ckpt --> .pt

    if not os.path.exists(pl_ckpt_path):
        convert_zero_checkpoint_to_fp32_state_dict(deepspeed_ckpt_path, pl_ckpt_path)

    # Patch in missing parameters that failed to be converted by DeepSpeed utility
    pl_ckpt = _merge_deepspeed_weights(deepspeed_ckpt_path, pl_ckpt_path)
    torch.save(pl_ckpt, pl_ckpt_path)

    return pl_ckpt_path

def _merge_deepspeed_weights(deepspeed_ckpt_path: str, fp32_ckpt_path: str):
    '''
    Merges tensors with keys in the DeepSpeed checkpoint but not in the fp32_checkpoint
    into the fp32 state dict.
    deepspeed_ckpt_path: Path to the DeepSpeed checkpoint folder.
    fp32_ckpt_path: Path to the reconstructed
    '''
    # This first part is based on pytorch_lightning.utilities.deepspeed.convert_zero_checkpoint_to_fp32_state_dict
    checkpoint_dir = ds_checkpoint_dir(deepspeed_ckpt_path)
    optim_files = get_optim_files(checkpoint_dir)
    optim_state = torch.load(optim_files[0], map_location='cpu')
    zero_stage = optim_state["optimizer_state_dict"]["zero_stage"]
    deepspeed_model_file = get_model_state_file(checkpoint_dir, zero_stage)

    # Start adding all parameters from DeepSpeed ckpt to generated PyTorch Lightning ckpt
    ds_ckpt = torch.load(deepspeed_model_file, map_location='cpu')
    ds_sd = ds_ckpt['module']

    fp32_ckpt = torch.load(fp32_ckpt_path, map_location='cpu')
    fp32_sd = fp32_ckpt['state_dict']

    for k, v in ds_sd.items():
        try:
            match = re.match(DS_PARAM_REGEX, k)
            param_name = match.group(1)
        except:
            print(f'Failed to extract parameter from DeepSpeed key {k}')
            continue

        v = v.to(torch.float32)
        if param_name not in fp32_sd:
            print(f'+ Adding parameter {param_name} from DeepSpeed state_dict to fp32_sd')
            fp32_sd[param_name] = v
        else:
            try:
                assert torch.allclose(v, fp32_sd[param_name], atol=1e-2)
                # print(f'-> Value for {param_name} already in fp32 state dict')
            except AssertionError:
                print(f'++ Updating fp32_sd[{param_name}] w/ DeepSpeed state_dict value')
                fp32_sd[param_name] = v

    # Only necessary if you want to load a model w/ HF in the future instead of using it with lightning
    # Need to revert back to original parameter names, which have been changed by lightning
    if args.hf_ckpt:
        print('Adapting state dict for Huggingface w/o Lightning...')
        fp32_ckpt['state_dict'] = {re.sub('^model.', '', k): v for k, v in fp32_ckpt['state_dict'].items()}

    return fp32_ckpt


def main(args):

    ckpt_path = '/home/jkeenan/low_resource_rdf2text/lightning_logs/' + args.ckpt_dir
    for root, dirs, files in os.walk(ckpt_path):
        for dir in dirs:
            if dir.endswith(".ckpt"):
                ckpt_path = os.path.join(root, dir)
                break
    pl_ckpt_path = convert_deepspeed_checkpoint(ckpt_path)


if __name__ == "__main__":
    parser = ArgumentParser()

    # Possible arguments
    parser.add_argument(
        "--ckpt_dir",
        default=None,
        type=str,
        required=True,
        help='Directory in lightining_logs where the checkpoint is stored'
    )
    parser.add_argument(
        "--hf_ckpt",
        action='store_true',
        help='Whether or not to create a checkpoint that will work for huggingface w/o lightning'
    )
    args = parser.parse_args()
    main(args)