import sys
# Append General path so we can import from the utils folder
sys.path.append('/home/jkeenan/low_resource_rdf2text/')
import json
import torch
from torch.utils.data import IterableDataset
from tqdm import tqdm
import itertools
import torchdata.datapipes as dp
from torchdata.datapipes.iter import IterDataPipe
import random
from utils.general import padding_func, mask_prep
from dataclasses import dataclass
from transformers.file_utils import PaddingStrategy
from transformers.modeling_utils import PreTrainedModel
from transformers.tokenization_utils_base import PreTrainedTokenizerBase
from typing import Optional, Union


def tokenize_data(data_files, tokenizer, data_path):
    '''
    Function to tokenize the RDF graphs and sentences of a given input

    Parameters:
        - data_files: dictionary containing the files to tokenize (note: this dictionary is defined in the process_data function)
        - tokenizer: tokenizer to use for tokenization
        - data_path: path to the files in the data_files dictionary

    Outputs:
        - data_files: dictionary containing the tokenized versions of the original data_files
    '''
    for split in tqdm(data_files, desc='Tokenizing data'):
        split_data = []
        for file in data_files[split]:
            with open(f'{data_path}/{file}', 'r') as f:
                split_data.append(f.read().splitlines())
        data_files[split] = split_data
        data_files[split][0] = tokenizer.batch_encode_plus(data_files[split][0],
                                                           return_special_tokens_mask=False,
                                                           add_special_tokens=False)['input_ids']
        data_files[split][1] = tokenizer.batch_encode_plus(data_files[split][1],
                                                           return_special_tokens_mask=False,
                                                           add_special_tokens=False)['input_ids']
        data_files[split] = list(zip(data_files[split][0], data_files[split][1]))
        data_files[split] = [{'rdf': item[0], 'sentence': item[1]} for item in data_files[split]]

    return data_files


def process_data(tokenizer, test=False, data_path=None):
    '''
    Function to organize to organize the tokenized data into one dictionary that can be used by the RDF2TextDataset class

    Parameters:
        - tokenizer: tokenizer to use for tokenization
        - test: boolean to indicate whether the data is for training or testing;
            if test=True we will preprocess train and validation data, otherwise we will just preprocess test data
        - data_path: path to the files in the data_files dictionary

    Outputs:
        - proc_data: dictionary containing the tokenized data for each dataset split
    '''
    proc_data = {}
    if test == False:
        data_files = {'train': ['train.source', 'train.target'],
                      'val': ['val.source', 'val.target']}

        data_tok = tokenize_data(data_files, tokenizer, data_path)

        proc_data['train'] = data_tok['train']
        proc_data['val'] = data_tok['val']

    elif test:
        data_files = {'test': ['test.source', 'test.target']}

        data_tok = tokenize_data(data_files, tokenizer, data_path)

        proc_data['test'] = data_tok['test']

    return proc_data


class CustomDataset(IterableDataset):
    def __init__(self, args, tokenizer, rank, world_size, stage='train'):
        super().__init__()
        self.args = args
        self.tokenizer = tokenizer
        self.filepath = self.args.data_path
        self.stage = stage
        self.rank = rank
        self.world_size = world_size
    
    def __iter__(self):
        assert self.stage in ['train', 'val', 'test']
        worker_info = torch.utils.data.get_worker_info()
        if worker_info is not None:
            num_workers = worker_info.num_workers
            worker_id = torch.utils.data.get_worker_info().id
            world_size = self.world_size
            rank = self.rank

        data = self.read_from_file()
        tok_itr = map(self.tokenize_inputs, data)

        if worker_info is not None:
            if rank == 0:
                tok_itr = itertools.islice(tok_itr, worker_id, None, (num_workers * world_size))
            else:
                tok_itr = itertools.islice(tok_itr, worker_id + (num_workers * rank), None, (num_workers * world_size))
        else:
           tok_itr = itertools.islice(tok_itr, self.rank, None, self.world_size)

        return tok_itr
    
    def read_from_file(self):
        if self.stage == 'train':
           source = open(f'{self.filepath}/train.source', encoding='utf-8')
           target = open(f'{self.filepath}/train.target', encoding='utf-8')
           if self.args.entity_lexicalization:
              source = open(f'{self.filepath}/train.entities', encoding='utf-8')
              target = open(f'{self.filepath}/train.mentions', encoding='utf-8')
           if self.args.mask_entities or self.args.mask_properties:
              ents = open(f'{self.filepath}/train_org.ents', encoding='utf-8')
              
        elif self.stage == 'val':
           source = open(f'{self.filepath}/val.source', encoding='utf-8')
           target = open(f'{self.filepath}/val.target', encoding='utf-8')
           if self.args.entity_lexicalization:
              source = open(f'{self.filepath}/val.entities', encoding='utf-8')
              target = open(f'{self.filepath}/val.mentions', encoding='utf-8')
           if self.args.mask_entities or self.args.mask_properties:
              ents = open(f'{self.filepath}/val_org.ents', encoding='utf-8')

        elif self.stage == 'test':
            source = open(f'{self.filepath}/test_both.source', encoding='utf-8')
            target = open(f'{self.filepath}/test_both.target', encoding='utf-8')
            if self.args.entity_lexicalization:
              source = open(f'{self.filepath}/test_both.entities', encoding='utf-8')
              target = open(f'{self.filepath}/test_both.mentions', encoding='utf-8')
            if self.args.mask_entities or self.args.mask_properties:
               ents = open(f'{self.filepath}/test_both_org.ents', encoding='utf-8')
        
        if self.args.mask_entities or self.args.mask_properties:
           yield from zip(source, target, ents)
        else:
           yield from zip(source, target)
        
    def no_newlines(self, lines):
        '''
        Function to take new lines out of inputs
        '''

        if self.args.mask_entities or self.args.mask_properties:
            ents = json.loads(lines[2])
            lines = [item.strip('\n') for item in lines[:2]]
            return lines, ents
        else:
           lines = [item.strip('\n') for item in lines]
           return lines

    def tokenize_inputs(self, lines):
        '''
        Function to tokenize a batch of lines that are read
        '''
        
        if self.args.mask_entities or self.args.mask_properties:
            graph_indicies = []
            sent_indicies = []
            
            lines, ents = self.no_newlines(lines)

            lines_tok = self.tokenizer.batch_encode_plus(lines,
                                                        return_special_tokens_mask=False,
                                                        add_special_tokens=False)

            graph_indicies = [(lines_tok.char_to_token(0, ind[0]), lines_tok.char_to_token(0, ind[1])) for ent in ents.keys() for ind in ents[ent]['graph_indicies'] if ents[ent]['graph_indicies']]
            sent_indicies = [(lines_tok.char_to_token(1, ind[0]), lines_tok.char_to_token(1, ind[1])) for ent in ents.keys() for ind in ents[ent]['sent_indicies'] if ents[ent]['sent_indicies']]
            '''
            nono = False
            for x in graph_indicies:
               if None in x:
                  print('NONE GRAPH')
                  nono = True
            for x in sent_indicies:
               if None in x:
                    print('NONE SENT')
                    nono = True
            if nono:
                print(lines)
                print(ents)
                print(graph_indicies)
                print(sent_indicies)
                print(lines_tok.tokens(1), '\n-----\n')
            '''
            lines_tok['graph_indicies'] = graph_indicies
            lines_tok['sent_indicies'] = sent_indicies
        
        else:
           lines = self.no_newlines(lines)

           lines_tok = self.tokenizer.batch_encode_plus(lines,
                                                        return_special_tokens_mask=False,
                                                        add_special_tokens=False)

        return lines_tok



class CustomDatapipe(IterDataPipe):
    def __init__(self, tokenizer, filepath, rank, world_size, stage='train'):
        super().__init__()
        self.tokenizer = tokenizer
        self.filepath = filepath
        self.stage = stage
        self.rank = rank
        self.world_size = world_size
    '''
    def __len__(self):
        if self.stage == 'train':
            file = open(f'{self.filepath}/train.source')
            length = len(file.readlines())
            file.close()
            return length
        elif self.stage == 'val':
            file = open(f'{self.filepath}/val.source')
            length = len(file.readlines())
            file.close()
            return length
        elif self.stage == 'test':
            file = open(f'{self.filepath}/test_both.source')
            length = len(file.readlines())
            file.close()
            return length
    '''
    
    def __iter__(self):
        assert self.stage in ['train', 'val', 'test']
        '''
        worker_info = torch.utils.data.get_worker_info()
        if worker_info is not None:
            num_workers = worker_info.num_workers
            worker_id = torch.utils.data.get_worker_info().id
            world_size = self.world_size
            rank = self.rank
            # init_process(rank=process_rank, num_processes=world_size)
        '''
        if self.stage == 'train':
            train_iter_source = open(f'{self.filepath}/train.source')
            train_iter_target = open(f'{self.filepath}/train.target')
            datapoint = zip(train_iter_source, train_iter_target)
            mapped_itr = map(self.no_newlines, datapoint)
            tok_itr = map(self.tokenize_inputs, mapped_itr)
        elif self.stage == 'val':
            val_iter_source = open(f'{self.filepath}/val.source')
            val_iter_target = open(f'{self.filepath}/val.target')
            datapoint = zip(val_iter_source, val_iter_target)
            mapped_itr = map(self.no_newlines, datapoint)
            tok_itr = map(self.tokenize_inputs, mapped_itr)
        elif self.stage == 'test':
            test_iter_source = open(f'{self.filepath}/test_both.source')
            test_iter_target = open(f'{self.filepath}/test_both.target')
            datapoint = zip(test_iter_source, test_iter_target)
            mapped_itr = map(self.no_newlines, datapoint)
            tok_itr = map(self.tokenize_inputs, mapped_itr)

        return tok_itr
        

    def no_newlines(self, lines):
        '''
        Function to take new lines out of inputs
        '''
        lines = list(lines)

        for idx, line in enumerate(lines):
            lines[idx] = line.strip('\n')

        return lines

    def tokenize_inputs(self, lines):
        '''
        Function to tokenize a batch of lines that are read
        '''
        lines_tok = self.tokenizer.batch_encode_plus(lines,
                                                return_special_tokens_mask=False,
                                                add_special_tokens=False)['input_ids']
        return lines_tok


def build_datapipes(tokenizer, filepath, rank, world_size, stage='train'):
    '''
    Function to build the datapipe
    '''
    datapipe = CustomDatapipe(tokenizer, filepath, rank, world_size, stage=stage)
    if stage == 'train':
        datapipe = dp.iter.Shuffler(datapipe)
    datapipe = dp.iter.Batcher(datapipe, batch_size=8)
    datapipe = dp.iter.ShardingFilter(datapipe)
    # datapipe = datapipe.apply_sharding(num_of_instances=world_size, instance_id=rank)
    # datapipe = datapipe.map(no_newlines)
    # datapipe = datapipe.map(tokenize_inputs, tokenizer=tokenizer)
    datapipe.apply_sharding(num_of_instances=world_size, instance_id=rank)

    return datapipe

class ShuffleDataset(IterableDataset):
  def __init__(self, dataset, buffer_size):
    super().__init__()
    self.dataset = dataset
    self.buffer_size = buffer_size

  def __iter__(self):
    shufbuf = []
    try:
      dataset_iter = iter(self.dataset)
      for i in tqdm(range(self.buffer_size), desc='Shuffling Dataset'):
        shufbuf.append(next(dataset_iter))
    except:
      self.buffer_size = len(shufbuf)

    try:
      while True:
        try:
          item = next(dataset_iter)
          evict_idx = random.randint(0, self.buffer_size - 1)
          yield shufbuf[evict_idx]
          shufbuf[evict_idx] = item
        except StopIteration:
          break
      while len(shufbuf) > 0:
        yield shufbuf.pop()
    except GeneratorExit:
      pass


@dataclass
class DataCollatorForSeq2SeqWithMaskingAndPadding:
    """
    Data collator that will preprocess data for masking and dynamically pad the inputs received, as well as the labels.
    Args:
        tokenizer (:class:`~transformers.PreTrainedTokenizer` or :class:`~transformers.PreTrainedTokenizerFast`):
            The tokenizer used for encoding the data.
        model (:class:`~transformers.PreTrainedModel`):
            The model that is being trained. If set and has the `prepare_decoder_input_ids_from_labels`, use it to
            prepare the `decoder_input_ids`
            This is useful when using `label_smoothing` to avoid calculating loss twice.
        padding (:obj:`bool`, :obj:`str` or :class:`~transformers.file_utils.PaddingStrategy`, `optional`, defaults to :obj:`True`):
            Select a strategy to pad the returned sequences (according to the model's padding side and padding index)
            among:
            * :obj:`True` or :obj:`'longest'`: Pad to the longest sequence in the batch (or no padding if only a single
              sequence is provided).
            * :obj:`'max_length'`: Pad to a maximum length specified with the argument :obj:`max_length` or to the
              maximum acceptable input length for the model if that argument is not provided.
            * :obj:`False` or :obj:`'do_not_pad'` (default): No padding (i.e., can output a batch with sequences of
              different lengths).
        max_length (:obj:`int`, `optional`):
            Maximum length of the returned list and optionally padding length (see above).
        pad_to_multiple_of (:obj:`int`, `optional`):
            If set will pad the sequence to a multiple of the provided value.
            This is especially useful to enable the use of Tensor Cores on NVIDIA hardware with compute capability >=
            7.5 (Volta).
        label_pad_token_id (:obj:`int`, `optional`, defaults to -100):
            The id to use when padding the labels (-100 will be automatically ignored by PyTorch loss functions).
    """

    tokenizer: PreTrainedTokenizerBase
    model: Optional[PreTrainedModel] = None
    padding: Union[bool, str, PaddingStrategy] = True
    max_length: Optional[int] = None
    pad_to_multiple_of: Optional[int] = None
    label_pad_token_id: int = -100

    def __call__(self, data):
        # Prepare the data so it can be fed into the masking functions
        features = mask_prep(data, self.tokenizer, self.max_length)

        # Pad all of the data
        padding_func(
            features,
            padding_side=self.tokenizer.padding_side,
            pad_token_id=self.label_pad_token_id,
            key="labels",
        )
        padding_func(
            features,
            padding_side=self.tokenizer.padding_side,
            pad_token_id=self.tokenizer.pad_token_id,
            key="joint_ids",
        )
        '''
        padding_func(
            features,
            padding_side=self.tokenizer.padding_side,
            pad_token_id=self.tokenizer.pad_token_id,
            key="joint_ids_2graph",
        )
        '''
        padding_func(
            features,
            padding_side=self.tokenizer.padding_side,
            pad_token_id=self.label_pad_token_id,
            key="seg_ids",
        )
        '''
        padding_func(
            features,
            padding_side=self.tokenizer.padding_side,
            pad_token_id=self.label_pad_token_id,
            key="seg_ids_2graph",
        )
        '''
        padding_func(
            features,
            padding_side=self.tokenizer.padding_side,
            pad_token_id=self.tokenizer.pad_token_id,
            key="sentErdf_ids",
        )
        padding_func(
            features,
            padding_side=self.tokenizer.padding_side,
            pad_token_id=self.label_pad_token_id,
            key="sentErdf_segids",
        )
        padding_func(
            features,
            padding_side=self.tokenizer.padding_side,
            pad_token_id=self.tokenizer.pad_token_id,
            key="rdfEsent_ids",
        )
        padding_func(
            features,
            padding_side=self.tokenizer.padding_side,
            pad_token_id=self.label_pad_token_id,
            key="rdfEsent_segids",
        )
        features = self.tokenizer.pad(
            features,
            padding=self.padding,
            return_tensors="pt",
        )

        batch_dict =  {
            "input_ids": features["input_ids"],
            "labels": features["labels"],
            "joint_ids": features["joint_ids"],
            # "joint_ids_2graph": features["joint_ids_2graph"],
            "seg_ids": features["seg_ids"],
            # "seg_ids_2graph": features["seg_ids_2graph"],
            "sentErdf_ids": features["sentErdf_ids"],
            "sentErdf_segids": features["sentErdf_segids"],
            "rdfEsent_ids": features["rdfEsent_ids"],
            "rdfEsent_segids": features["rdfEsent_segids"],
        }

        if "sent_indicies" in data[0].keys() or "graph_indicies" in data[0].keys():
           '''
           If 'sent_indicies' or 'graph_indicies' are in the keys of one of the datapoints,
           then we know that entity_masking is True and we should pass these datapoints onto
           the next step
           '''
           batch_dict['sent_indicies'] = [item['sent_indicies'] for item in data]
           batch_dict['graph_indicies'] = [item['graph_indicies'] for item in data]

        return batch_dict
    
